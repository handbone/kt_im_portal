<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />


    <c:if test="${ resultType eq 'contentsGenreList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsGenreList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="genreSeq" value="${ i.genreSeq }"/>
                        <json:property name="genreNm" value="${ i.genreNm }" escapeXml="false"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>


      <c:if test="${ resultType eq 'contentsInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="contentsInfo">
                    <json:property name="contsSeq" value="${ item.contsSeq }"/>
                    <json:property name="contsTitle" value="${ item.contsTitle }" escapeXml="false"/>
                    <json:property name="contsSubTitle" value="${ item.contsSubTitle }" escapeXml="false"/>
                    <json:property name="contsDesc" value="${ item.contsDesc }" escapeXml="false"/>
                    <json:property name="contsID" value="${ item.contsID }"/>
                    <json:property name="firstCtgNm" value="${ item.firstCtgNm }"/>
                    <json:property name="secondCtgNm" value="${ item.secondCtgNm }"/>
                    <json:property name="rcessWhySbst" value="${ item.rcessWhySbst }"/>
                    <json:property name="cretrID" value="${ item.cretrID }"/>
                    <json:property name="fileType" value="${ item.fileType }"/>
                    <json:property name="fileSize" value="${ item.fileSize }"/>
                    <json:property name="maxAvlNop" value="${ item.maxAvlNop }"/>
                    <json:property name="exeFilePath" value="${ item.exeFilePath }"/>
                    <json:property name="mbrNm" value="${ item.mbrNm }"/>
                    <json:property name="contsVer" value="${ item.contsVer }"/>
                    <json:property name="contsCtgSeq" value="${ item.contsCtgSeq }"/>
                    <json:property name="sttus" value="${ item.sttusNm }"/>
                    <json:property name="sttusVal" value="${ item.sttusVal }"/>
                    <json:property name="cpNm" value="${ item.cpNm }"/>
                    <json:property name="cntrctStDt" value="${ item.cntrctStDt }"/>
                    <json:property name="cntrctFnsDt" value="${ item.cntrctFnsDt }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                    <json:property name="amdrID" value="${ item.amdrID }"/>
                    <json:property name="coverImgfileSeq" value="${ item.fileSeq }"/>
                    <json:property name="coverImgNm" value="${ item.orginlFileNm }"/>
                    <json:property name="coverImgPath" value="${ item.filePath }"/>
                    <json:array name="thumbnailList" items="${thumbnailList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="thumbnailNm" value="${ i.orginlFileNm }"/>
                            <json:property name="thumbnailPath" value="${ i.filePath }"/>
                        </json:object>
                    </json:array>
                    <c:if test="${ item.fileType eq 'VIDEO'}">
                    <json:array name="videoList" items="${videoList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                            <json:object name="metadataInfo">
                                <json:property name="fileSeq" value="${ i.metadataXmlInfo.fileSeq}"/>
                                <json:property name="fileNm" value="${ i.metadataXmlInfo.orginlFileNm }"/>
                                <json:property name="filePath" value="${ i.metadataXmlInfo.filePath }"/>
                            </json:object>
                            <json:object name="contsXmlInfo">
                                <json:property name="fileSeq" value="${ i.contsXmlInfo.fileSeq }"/>
                                <json:property name="fileNm" value="${ i.contsXmlInfo.orginlFileNm }"/>
                                <json:property name="filePath" value="${ i.contsXmlInfo.filePath }"/>
                            </json:object>
                        </json:object>
                    </json:array>
                    </c:if>
                    <json:array name="prevList" items="${prevList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                        </json:object>
                    </json:array>
                    <c:if test="${ item.fileType ne 'VIDEO'}">
                    <json:array name="fileList" items="${fileList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                        </json:object>
                    </json:array>
                    </c:if>
                    <json:array name="genreList" items="${genreList}" var="i">
                        <json:object>
                            <json:property name="genreSeq" value="${ i.genreSeq }"/>
                            <json:property name="genreNm" value="${ i.genreNm }" escapeXml="false"/>
                        </json:object>
                    </json:array>
                    <json:array name="serviceList" items="${serviceList}" var="i">
                        <json:object>
                            <json:property name="svcSeq" value="${ i.svcSeq }"/>
                            <json:property name="svcNm" value="${ i.svcNm }" escapeXml="false"/>
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
      </c:if>

    <c:if test="${ resultType eq 'contentDisplayList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentDisplayList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="fileSeq" value=" ${ i.fileSeq }" />
                        <json:property name="filePath" value=" ${ i.filePath }" />
                        <json:property name="orginlFileNm" value=" ${ i.orginlFileNm }" />
                        <json:property name="contsID" value=" ${ i.contsID }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsDesc" value=" ${ i.contsDesc }" escapeXml="false"/>
                        <json:property name="contsSubTitle" value="${ i.contsSubTitle }" escapeXml="false"/>
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="genreNm" value=" ${ i.genreNm }" escapeXml="false" />
                        <json:property name="cretrID" value=" ${ i.cretrID }" />
                        <json:property name="cretrNm" value=" ${ i.cretrNm }" />
                        <json:property name="sttusVal" value=" ${ i.sttusVal }" />
                        <json:property name="maxAvlNop" value=" ${ i.maxAvlNop }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'recommendContentList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="recommendContentList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="rcmdContsSeq" value=" ${ i.rcmdContsSeq }" />
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="filePath" value=" ${ i.filePath }" />
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="genreNm" value=" ${ i.genreNm }" escapeXml="false" />
                        <json:property name="contsDesc" value=" ${ i.contsDesc }" escapeXml="false"/>
                        <json:property name="contsSubTitle" value="${ i.contsSubTitle }" escapeXml="false"/>
                        <json:property name="maxAvlNop" value=" ${ i.maxAvlNop }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsCtgList' }">
      <c:if test="${ resultCode eq '1000' }">
          <json:object name="result">
              <c:if test="${ !empty firstCtgList}">
                  <json:array name="firstCtgList" items="${firstCtgList}" var="i">
                      <json:object>
                          <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                          <json:property name="firstCtgID" value="${ i.firstCtgID }"/>
                      </json:object>
                  </json:array>
              </c:if>
              <json:array name="contsCtgList" items="${item}" var="i">
                  <json:object>
                      <json:property name="contsCtgSeq" value="${ i.contsCtgSeq }"/>
                      <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                      <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                      <json:property name="firstCtgID" value="${ i.firstCtgID }"/>
                      <json:property name="secondCtgID" value="${ i.secondCtgID }"/>
                      <json:property name="ctgCnt" value="${ i.ctgCnt+1 }"/>
                      <json:property name="cretDt" value="${ i.cretDt }"/>
                  </json:object>
              </json:array>
          </json:object>
      </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsCtgListNm' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contsCtgList" items="${item}" var="i">
                    <json:object>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                        <json:property name="secondCtgID" value="${ i.secondCtgID }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'ctgInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="ctgInfo">
                    <json:property name="firstCtgID" value="${ item.firstCtgID }"/>
                    <json:property name="firstCtgNm" value="${ item.firstCtgNm }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

</json:object>