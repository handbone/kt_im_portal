<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:set var="count" value="0"></c:set>

    <c:if test="${ resultType eq 'noticeList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="noticeList" items="${item}" var="i">
                    <json:object>
                        <c:if test="${i.noticePrefRank ne '0'}">
                            <json:property name="num" value="" />
                        </c:if>
                        <c:if test="${i.noticePrefRank eq '0'}">
                            <json:property name="num" value="${(totalCount - imptTotCnt) - ((currentPage-1) * row + count)}" />
                        </c:if>
                        <json:property name="noticePrefRank" value="${ i.noticePrefRank }" />
                        <json:property name="noticeSeq" value="${ i.noticeSeq }" />
                        <json:property name="noticeTitle" value="${ i.noticeTitle }" escapeXml="false" />
                        <c:set var="fileExist" value="false" />
                        <c:if test="${i.fileCount > 0}">
                            <c:set var="fileExist" value="true" />
                        </c:if>
                        <json:property name="fileExist" value="${fileExist}" />
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:if test="${i.noticePrefRank eq '0'}">
                            <c:set var="count" value="${count + 1 }"></c:set>
                        </c:if>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${(totalCount - imptTotCnt) == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${(totalCount - imptTotCnt) % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage + 1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'noticeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="noticeDetail">
                    <json:property name="noticeSeq" value="${ item.noticeSeq }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="noticeTitle" value="${ item.noticeTitle }" escapeXml="false" />
                    <json:property name="noticeSbst" value="${ item.noticeSbst }" escapeXml="true" />
                    <json:property name="retvNum" value="${ item.retvNum }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                </json:object>
                <json:array name="fileList" items="${fileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>