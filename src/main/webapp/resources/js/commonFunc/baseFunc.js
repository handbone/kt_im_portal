/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

function setStyle(id,style,value)
{
    id.style[style] = value;
}
function opacity(el,opacity)
{
        setStyle(el,"filter:","alpha(opacity="+opacity+")");
        setStyle(el,"-moz-opacity",opacity/100);
        setStyle(el,"-khtml-opacity",opacity/100);
        setStyle(el,"opacity",opacity/100);
}
function calendar()
{
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getYear();
        if(year<=200)
        {
                year += 1900;
        }
        months = new Array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');
        days_in_month = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
        if(year%4 == 0 && year!=1900)
        {
                days_in_month[1]=29;
        }
        total = days_in_month[month];
        var date_today = year+'년'+months[month]+'월 '+day+'일';
        beg_j = date;
        beg_j.setDate(1);
        if(beg_j.getDate()==2)
        {
                beg_j=setDate(0);
        }
        beg_j = beg_j.getDay();
        document.write('<table class="cal_calendar" onload="opacity(document.getElementById(\'cal_body\'),20);"><tbody id="cal_body"><tr><th colspan="7">'+date_today+'</th></tr>');
        document.write('<tr class="cal_d_weeks"><th>일</th><th>월</th><th>화</th><th>수</th><th>목</th><th>금</th><th>토</th></tr><tr>');
        week = 0;
        for(i=1;i<=beg_j;i++)
        {
                document.write('<td class="cal_days_bef_aft">'+(days_in_month[month-1]-beg_j+i)+'</td>');
                week++;
        }
        for(i=1;i<=total;i++)
        {
                if(week==0)
                {
                        document.write('<tr>');
                }
                if(day==i)
                {
                        document.write('<td class="cal_today" title="오늘">'+i+'</td>');
                }
                else
                {
                        document.write('<td>'+i+'</td>');
                }
                week++;
                if(week==7)
                {
                        document.write('</tr>');
                        week=0;
                }
        }
        for(i=1;week!=0;i++)
        {
                document.write('<td class="cal_days_bef_aft">'+i+'</td>');
                week++;
                if(week==7)
                {
                        document.write('</tr>');
                        week=0;
                }
        }
        document.write('</tbody></table>');
        opacity(document.getElementById('cal_body'),70);
        return true;
}



//푸터영역 정의
function footPosition() {
    var contentsHeight = $('#Contents').outerHeight(true);
    //console.log(contentsHeight);
      var displayHeight;
       var pageHeight = parseInt(contentsHeight) + 160;
       var topoffset = parseInt($(window).height());
       var ruleHeight = "685";
       if(contentsHeight > 685){ ruleHeight = pageHeight; }

    if( topoffset >= ruleHeight ){
           displayHeight = parseInt(contentsHeight) +(parseInt(topoffset) - parseInt(pageHeight));
       //    $("#contentsWrap").css({'height': displayHeight +'px'});
    } else{
           displayHeight = contentsHeight;
           //$("#contentsWrap").css({'height': '100%'});
    }
    /*if(displayHeight < 667){
        displayHeight = 567;
    }*/
    $("#contentsWrap").css({'height': displayHeight +'px'});
       return;
}

function setComma(str)
 {
        str = ""+str+"";
        var retValue = "";
        for(i=0; i<str.length; i++)
        {
                if(i > 0 && (i%3)==0) {
                        retValue = str.charAt(str.length - i -1) + "," + retValue;
                 } else {
                        retValue = str.charAt(str.length - i -1) + retValue;
                }
        }
        return retValue +"원";
 }

function commonFunction(){

    $(window).resize(function(){ 
        footPosition(); 
    });
    //$('input, textarea').placeholder();
}

function APIComplateFunction(){
    footPosition(); 
    //$('input, textarea').placeholder();
}

function dateConvert(date){
    var arr = new Object()
    var item= date.split(" ");
    var yymmdd = item[0].split("-");
    var yy = yymmdd[0];
    var mm = yymmdd[1]-1;
    var dd = yymmdd[2];
    var hms = item[1].split(".")[0].split(":");
    var h = hms[0];
    var m = hms[1];
    var s = hms[2];

    arr["yy"] = yy;
    arr["mm"] = mm;
    arr["dd"] = dd;
    arr["yy"] = yy;
    arr["h"] = h;
    arr["m"] = m;
    arr["s"] = s;

    return arr;
}

function dateConvert2(date){
    if(date){
        var arr = new Object()
        var item= date.split(" ");
        var yymmdd = item[0].split("-");
        var yy = yymmdd[0];
        var mm = yymmdd[1]-1;
        var dd = yymmdd[2];
        var hms = item[1].split(":");
        var h = hms[0];
        var m = hms[1];
        var s = hms[2];

        arr["yy"] = yy;
        arr["mm"] = mm;
        arr["dd"] = dd;
        arr["yy"] = yy;
        arr["h"] = h;
        arr["m"] = m;
        arr["s"] = s;

        return arr;
    }
}

function dateConvert3(date){
    var arr = new Object()
    var item= date.split(" ");
    var yymmdd = item[0].split("-");
    var yy = yymmdd[0];
    var mm = yymmdd[1];
    var dd = yymmdd[2];
    var hms = item[1].split(":");
    var h = hms[0];
    var m = hms[1];
    var s = hms[2];

    arr["yy"] = yy;
    arr["mm"] = mm;
    arr["dd"] = dd;
    arr["yy"] = yy;
    arr["h"] = h;
    arr["m"] = m;
    arr["s"] = s;

    return arr;
}

function dateConvert4(date){
    var arr = new Object()
    var yymmdd = date.split("-");
    var yy = yymmdd[0];
    var mm = yymmdd[1];

    arr["yy"] = yy;
    arr["mm"] = mm;

    return arr;
}

Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";

    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;

    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);}; 

function dateBasicFomet(date){
    var pre_date = dateConvert2(date);
    return new Date(Math.floor(new Date(pre_date["yy"], pre_date["mm"], pre_date["dd"], pre_date["h"], pre_date["m"], pre_date["s"]).getTime()));
}

function dateFullCalendarFomet(date){
    var pre_date = dateConvert3(date);
    return pre_date["yy"]+"-"+pre_date["mm"]+"-"+pre_date["dd"]+"T"+pre_date["h"]+":"+pre_date["m"]+":"+pre_date["s"];
}

function dateFullCalendarFomet_end(date){
    var pre_date = dateConvert2(date);
    var e_Time = new Date(pre_date["yy"], pre_date["mm"], pre_date["dd"], pre_date["h"], pre_date["m"], pre_date["s"]);
    if(new Date(e_Time).format("a/p") == "오전" ){
        if(Number(new Date(e_Time).format("hh"))>8){
            return new Date(e_Time).format("yyyy-MM-ddThh:mm:ss");
        }else{
            return new Date(e_Time.setDate(e_Time.getDate()+1)).format("yyyy-MM-ddThh:mm:ss");
        }
    }else{
        return new Date(e_Time.setDate(e_Time.getDate()+1)).format("yyyy-MM-ddThh:mm:ss");
    }
}

function dateFullCalendarFomet2(startDate, endDate){
    var s_date = dateConvert2(startDate);
    var e_date = dateConvert2(endDate);
    var s_Time = new Date(s_date["yy"], s_date["mm"], s_date["dd"], s_date["h"], s_date["m"], s_date["s"]);
    var e_Time = new Date(e_date["yy"], e_date["mm"], e_date["dd"], e_date["h"], e_date["m"], e_date["s"]);
    if(s_date["yy"] == e_date["yy"] && s_date["mm"] == e_date["mm"] && s_date["dd"] == e_date["dd"]){
        return new Date(s_Time).format("yyyy년 MM월 dd일 a/p hh:mm")+" - "+new Date(e_Time).format("a/p hh:mm");
    }else{
        return new Date(s_Time).format("yyyy년 MM월 dd일 a/p hh:mm")+" - "+new Date(e_Time).format("dd일 a/p hh:mm");
    }
}

function dateFullCalendarFomet3(startDate, alarmDate){
    var s_date = dateConvert2(startDate);
    var e_date = dateConvert2(alarmDate);
    var s_Time = new Date(s_date["yy"], s_date["mm"], s_date["dd"], s_date["h"], s_date["m"], s_date["s"]);
    var e_Time = new Date(e_date["yy"], e_date["mm"], e_date["dd"], e_date["h"], e_date["m"], e_date["s"]);
    var min = (s_Time.getTime() - e_Time.getTime())/60000;
    if(min < 60){
        return min +"분전 알림";
    }else if(min < 1440){
        return min/60 +"시간전 알림";
    }else{
        return min/1444 +"일전 알림";
    }
}

function dateFullCalendarFomet7(startDate, alarmDate){
    var s_date = dateConvert2(startDate);
    var e_date = dateConvert2(alarmDate);
    var s_Time = new Date(s_date["yy"], s_date["mm"], s_date["dd"], s_date["h"], s_date["m"], s_date["s"]);
    var e_Time = new Date(e_date["yy"], e_date["mm"], e_date["dd"], e_date["h"], e_date["m"], e_date["s"]);
    return (s_Time.getTime() - e_Time.getTime())/60000;
}

function dateFullCalendarFomet4(date){
    if(date){
        var r_date = dateConvert2(date);
        var s_Time = new Date(r_date["yy"], r_date["mm"], r_date["dd"], r_date["h"], r_date["m"], r_date["s"]);
        return new Date(s_Time).format("yyyy년 MM월 dd일 E");
    }
}

function dateFullCalendarFomet5(date){
    var r_date = dateConvert2(date);
    var s_Time = new Date(r_date["yy"], r_date["mm"], r_date["dd"], r_date["h"], r_date["m"], r_date["s"]);
    return new Date(s_Time).format("a/p hh:mm");
}

function dateFullCalendarFomet6(date){
    var r_date = dateConvert2(date);
    var s_Time = new Date(r_date["yy"], r_date["mm"], r_date["dd"], r_date["h"], r_date["m"], r_date["s"]);
    return new Date(s_Time).format("MM월 dd일 a/p hh:mm");
}

function nowDateYm(){
    return new Date().format("yyyy-MM");
}

function trim(str) {
    str = input.replace(/(^\s*)|(\s*$)/, "");
    return str;
}

function preDateYm(date){
    var pre_date = dateConvert4(date);
    var Time = new Date(pre_date["yy"], pre_date["mm"]);
    Time.setMonth(Time.getMonth()-2);
    return new Date(Time).format("yyyy-MM");
}

function nextDateYm(date){
    var pre_date = dateConvert4(date);
    var Time = new Date(pre_date["yy"], pre_date["mm"]);
    Time.setMonth(Time.getMonth());
    return new Date(Time).format("yyyy-MM");
}

function dateMinusFomet(date,min){
    var pre_date = dateConvert2(date);
    var Time = new Date(pre_date["yy"], pre_date["mm"], pre_date["dd"], pre_date["h"], pre_date["m"], pre_date["s"]);
    Time.setMinutes(Time.getMinutes() - min);
    return new Date(Time).format("yyyy-MM-dd HH:mm:ss");
}

function dateFomet(date, nowDate){
    var pre_date = dateConvert(date);
    var now_date = dateConvert(nowDate);

    var now = new Date(Math.floor(new Date(now_date["yy"], now_date["mm"], now_date["dd"], now_date["h"], now_date["m"], now_date["s"]).getTime()));
    var currentDate = new Date(Math.floor(new Date(pre_date["yy"], pre_date["mm"], pre_date["dd"], pre_date["h"], pre_date["m"], pre_date["s"]).getTime()));
    var diffMil = (now.getTime()) - (currentDate.getTime());
    var diffSec = parseInt(diffMil/1000),
        diffMin = parseInt(diffSec / 60),
        diffHour = parseInt(diffMin / 60),
        diffDays = parseInt(diffHour / 24),
        diffMonth = parseInt(diffDays / 30),
        diffYear = parseInt(diffDays / 365);
    if (isNaN(diffMil) || diffMil < 0) {
        return "";
    }
    if(diffDays > 0 ) return foo(Math.floor(new Date(pre_date["yy"], pre_date["mm"], pre_date["dd"], pre_date["h"], pre_date["m"], pre_date["s"]).getTime()));
    else if(diffHour > 0 ) return diffHour+"시간 전";
    else if(diffMin > 0) return diffMin+"분 전";
    else if(diffSec > 20) return diffSec+"초 전";
    else if(diffSec >= 0) return "방금";
    else return "방금";
}

function foo(timestamp){
    var date = new Date(timestamp);
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    var day = date.getDay();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    var retVal =   year + "-" + (month < 10 ? "0" + month : month) + "-" 
                            + (day < 10 ? "0" + day : day) + " " 
                            + (hour < 12 ? "오전 " + hour : "오후 " +hour) + ":"
                            + (min < 10 ? "0" + min : min);
    return retVal
}
