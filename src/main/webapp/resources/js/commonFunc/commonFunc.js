/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var isDevice = "";
var preDevice = "";

$(window).resize(function(){
    if (window.innerWidth <= 640) {
        isDevice = "mobile";
    } else {
        isDevice = "pc";
    }
});

$(document).ready(function(){
    var screenW = window.innerWidth;
    if (screenW > 640) {
        isDevice = preDevice = "pc";
    } else {
        isDevice = preDevice = "mobile";
    }
});

var reloadFlag = true;

/**
 * 페이지 이동 함수
 * @param {Object} url  URL 주소 값
 */
function pageMove(url){
    //defer 가져오기
    var str_hash = document.location.hash.replace("#","");
    var chk = "default";

    var isMobile = {
            Android: function () {return navigator.userAgent.match(/Android/i) == null ? false : true;},
            BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i) == null ? false : true;},
            IOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i) == null ? false : true;},
            Opera: function () {return navigator.userAgent.match(/Opera Mini/i) == null ? false : true;},
            Windows: function () {return navigator.userAgent.match(/IEMobile/i) == null ? false : true;},
            any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.IOS() || isMobile.Opera() || isMobile.Windows());}
        };
    if(isMobile.any()){
        chk = "mobile";
        if(isMobile.Android()){
        }else if(isMobile.IOS()){
        }else if(isMobile.BlackBerry()){
        }else if(isMobile.Opera()){
        }else if(isMobile.Windows()){
        }
    }

    if (chk == "mobile" && str_hash.indexOf('nMenuVw') != -1) {
        url += "#nMenuVw";
    } else if (chk == "mobile" && str_hash.indexOf('nMenuSw') != -1) {
        url += "#nMenuSw";
    }


    location.href = makeAPIUrl(url);
}
function pageMove_reload(url){
    //defer 가져오기
    location.href = makeAPIUrl(url);
    if(reloadFlag){
        location.reload();
        reloadFlag = false
    }
}

/**
 * Window Resize Event 발생 시에 Popup Layer 위치 변경
 */
function popupResize(layerId) {
    var wHeight = $(window).height();
    var wWidth = $(window).width();
    var sTop = document.body.scrollTop == 0 ? document.documentElement.scrollTop : document.body.scrollTop;
    var sLeft = document.body.scrollLeft == 0 ? document.documentElement.scrollLeft : document.body.scrollLeft;

    /*console.log(sTop+"/"+sLeft);
    console.log(wHeight+"/"+wWidth);*/
    var offsetTop = (wHeight - $("#" + layerId).height()) /2 + sTop;
    var offsetLeft = (wWidth - $("#" + layerId).width()) /2 + sLeft;
    offsetTop = offsetTop > 0 ? offsetTop : 0;
    offsetLeft = offsetLeft > 0 ? offsetLeft : 0;

    $("#" + layerId).offset({
        top: offsetTop,
        left: offsetLeft
    });

}

/**
 * Vertical Middle, Horizontal Center Popup Layer Load
 * (페이지 내 DIV 영역을 popup으로 띄움)
 * @param layerId : DIV id
 * @param width : Popup Layer width
 * @param height : Popup Layer height
 * @param changePosition : Window Resize Event 시에 위치 변경 여부
 */
function popLayerDiv(layerId, width, height, changePosition)
{
    var offsetTop = ($(window).height() - height) /2;
    var offsetLeft = ($(window).width() - width) /2;
    offsetTop = (offsetTop > 0)? offsetTop:0;
    offsetLeft = (offsetLeft > 0)? offsetLeft:0;
    $.blockUI({
        message: $("#" + layerId),
        css: {
            border: 'none',
            top:  offsetTop +300+'px',
            left: offsetLeft+150+ 'px',
            width: '0px',
            height: '0px',
            textAlign    : "",
            cursor:null
            },
        overlayCSS:  {
            backgroundColor: '#fff',
            opacity: 0.8
        }
    });
    $("body").css("overflow-y" , "hidden");
    $("body").children().filter(".blockUI").css("cursor", "default");
    if(changePosition)
    {
        popupResize(layerId);
        $(window).resize(function(){
            if($("#" + layerId).length == 1 && $("#" + layerId).css("display")=="block")
            {
                popupResize(layerId);
            }
        });
    }
}


/**
 * Popup Layer 닫기
 */
function popLayerClose()
{
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}


/**
 * Page Navigation 그리기
 *
 * @param totCnt    총 건수
 */
var _blockSize = 10;
function drawPaging(totCnt,offset,limit,searchFunc,pagingDiv) {
    pageNo = toInt(offset) / toInt(limit) + 1;
    pageSize = toInt(limit);
    var totPageCnt = toInt(totCnt / pageSize) + (totCnt % pageSize > 0 ? 1 : 0);
    var totBlockCnt = toInt(totPageCnt / _blockSize) + (totPageCnt % _blockSize > 0 ? 1 : 0);
    var blockNo = toInt(pageNo / _blockSize) + (pageNo % _blockSize > 0 ? 1 : 0);
    var startPageNo = (blockNo - 1) * _blockSize + 1;
    var endPageNo = blockNo * _blockSize;

    if (endPageNo > totPageCnt) {
        endPageNo = totPageCnt;
    }
    var prevBlockPageNo = (blockNo - 1) * _blockSize;
    var nextBlockPageNo = blockNo * _blockSize + 1;

    var strHTML = "<ul>";
    if (totPageCnt > 1 && pageNo != 1) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(1);\"><img src=\"/resources/image/btn_fast_rewind.gif\" width=\"15\" height=\"15\"></a></li>";

    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\"><img src=\"/resources/image/btn_fast_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    if (pageNo > 1) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + (pageNo-1) + ");\" ><img src=\"/resources/image/btn_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    var numberStyle = "", numberClass = "";
    for (var i = startPageNo; i <= endPageNo; i++) {
        numberStyle = (i == pageNo) ? "font-weight:bold;  letter-spacing:-1px;" : "";
        strHTML += "<li><a href=\"javascript:"+searchFunc+"(" + i + ");\" style='color:#3677b2;" + numberStyle + "'>" + i + "</a></li>";

    }
    if (totCnt == 0) {
        strHTML += "<li><a href=\"javascript:search(1);\">1</a></li>";

    }
    if (pageNo < totPageCnt) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + (pageNo+1) + ");\" ><img src=\"/resources/image/btn_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    if (totPageCnt > 1 && pageNo != totPageCnt) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + totPageCnt + ");\" ><img src=\"/resources/image/btn_fast_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_fast_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    }

    strHTML += "</ul>";
    $('#'+pagingDiv).html(strHTML);

}
/**
 * 검색 API offset parameter 값 계산
 *
 * @param pageNo    페이지번호
 * @return offset
*/
function getSearchOffset(pageNo) {
    return (pageNo - 1) * toInt($("#limit").val());
}


function toInt(str) {
    var n = null;
    try {
        n = parseInt(str, 10);
    } catch (e) {}
    return n;
}


var CALL_COUNT            = 0;
var CALL_SUCCESS_COUNT    = 0;
var CALL_TOT_COUNT        = 0;
var alertFlag = true;
function jsCallComplete() {
    if ( CALL_COUNT == CALL_TOT_COUNT ) {
        if ( CALL_SUCCESS_COUNT == CALL_COUNT ) {
            if(alertFlag){
                alertFlag = false;
                contentsXmlCreate();
            }
        }else{
            setTimeout("jsCallComplete()", 100);
        }
    }
    else {
        setTimeout("jsCallComplete()", 100);
    }
}
function jsCallComplete1() {
    if ( CALL_COUNT == CALL_TOT_COUNT ) {
        if ( CALL_SUCCESS_COUNT == CALL_COUNT ) {
            if(alertFlag){
                alertFlag = false;
                contentsXmlCreate();
            }
        }else{
            setTimeout("jsCallComplete1()", 100);
        }
    }
    else {
        setTimeout("jsCallComplete1()", 100);
    }
}


function strConv(str){
    str = str.replace(/&lt;/gi,"<");
    str = str.replace(/&gt;/gi,">");
    str = str.replace(/&quot;/gi,"\"");
    //str = str.replace(/&nbsp;/gi," ");
    str = str.replace(/&amp;/gi,"&");
    str = str.replace(/&amp;#034;/gi,"\"");
    str = str.replace(/&#034;/gi, "\"");
    str = str.replace(/\n/g, "<br>");
    return str;
}



//jqgrid 포인터
function pointercursor(cellvalue, options, rowObject)
{
    var new_formatted_cellvalue = '<a class="pointer">' + cellvalue + '</a>';

    return new_formatted_cellvalue;
}

function userInfoValidateCheck(type, memberType){
    // 등록일때
    if(type == "r"){
        if($("#memberId").val() == ""){
            popAlertLayer("아이디를 입력하세요");
            return false;
        } else if(!duplicationFlag){
            popAlertLayer("아이디를 중복체크해주세요.");
            return false;
        }

        if($("#memberPwd").val() == ""){
            popAlertLayer("비밀번호를 입력해주세요.");
            return false;
        } else if($("#memberPwd").val() !=$("#memberPwdRe").val()){
            popAlertLayer("비밀번호가 일치하지 않습니다.");
            return false;
        } else {
            var pwdRegular = /[a-z|0-9]{6,15}$/gi;
            if(!pwdRegular.test($("#memberPwd").val())){
                popAlertLayer("비밀번호 입력 양식을 확인하여 주십시오.");
                $("#memberPwd").val("");
                $("#pwdValueChk").css("display", "block");
                return false;
            } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
                $("#pwdValueChk").css("display", "block");
                return false;
            }
        }
    }
    // 수정일 때
    else if(type == "e"){
        if($("#memberPwd").val() != ""){
            if($("#memberPwd").val() !=$("#memberPwdRe").val()){
                popAlertLayer("비밀번호가 일치하지 않습니다.");
                return false;
            } else {
                var pwdRegular = /[a-z|0-9]{6,15}$/gi;
                if(!pwdRegular.test($("#memberPwd").val())){
                    popAlertLayer("비밀번호 입력 양식을 확인하여 주십시오.");
                    $("#pwdValueChk").css("display", "block");
                    $("#memberPwd").val("");
                    return false;
                } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
                    $("#pwdValueChk").css("display", "block");
                    return false;
                }
            }
        }
    }
    $("#pwdValueChk").css("display", "none");


    if($("#memberName").val() == ""){
        popAlertLayer("고객명을 입력해주세요.");
        return false;
    } else {
        var nameRegular = /^[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]{2,10}$/gi;
        if(!nameRegular.test($("#memberName").val())){
            popAlertLayer("고객명 입력 양식을 확인하여 주십시오.");
            $("#memberName").val("");
            return false;
        }
    }

    // 관리자일때
    if(memberType == "admin"){
        var phoneRegular1 = /[0-9]{2,3}/;
        var phoneRegular2 = /[0-9]{3,4}/;
        var phoneRegular3 = /[0-9]{4}/;
        if(!phoneRegular1.test($("#memberPhone1").val()) || !phoneRegular2.test($("#memberPhone2").val())
            || !phoneRegular3.test($("#memberPhone3").val())){
            popAlertLayer("전화번호를 정확히 입력해 주십시오.");
            return false;
        }
    }

    var mobilePhoneRegular1 = /[0-9]{3}/;
    var mobilePhoneRegular2 = /[0-9]{3,4}/;
    var mobilePhoneRegular3 = /[0-9]{4}/;
    if(!mobilePhoneRegular1.test($("#mobilePhone1").val()) || !mobilePhoneRegular2.test($("#mobilePhone2").val())
        || !mobilePhoneRegular3.test($("#mobilePhone3").val())){
        popAlertLayer("휴대폰 번호를 정확히 입력해 주십시오.");
        return false;
    }

    var email1Regular = /[a-z|0-9]$/gi;
    if(!email1Regular.test($("#memberEmail1").val())){
        popAlertLayer("이메일 입력 양식을 확인하여 주십시오.");
        $("#memberEmail1").val("");
        return false;
    }
    var email2Regular = /([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+)$/;
    if(!email2Regular.test($("#memberEmail2").val())){
        popAlertLayer("이메일 입력 양식을 확인하여 주십시오.");
        $("#memberEmail2").val("");
        return false;
    }
    return true;
}

function userPwdChange(){
    var pwdRegular = /[a-z|0-9]{6,15}$/gi;
    if(!pwdRegular.test($("#memberPwd").val())){
        $("#pwdValueComplete").text("사용 불가능한 비밀번호입니다.");
        $("#memberPwd").val("");
        return;
    } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
        $("#pwdValueComplete").text("사용 불가능한 비밀번호입니다.");
        $("#memberPwd").val("");
        return;
    } else {
        $("#pwdValueComplete").text("사용 가능한 비밀번호입니다.");
    }

}

function cutString(object, maxLength){
    var strLength = 0;
    var newStr = '';
    var str = "";
    if(typeof object == "object"){
        str = object.value;
    } else {
        str = object;
    }

    for (var i=0;i<str.length; i++) {
        var n = str.charCodeAt(i);
        var nv = str.charAt(i); // charAt : string 개체로부터 지정한 위치에 있는 문자를 꺼낸다.
        if ((n>= 0)&&(n<256)) {
            strLength ++; // ASCII 문자코드 set.
        } else {
            strLength += 2; // 한글이면 2byte로 계산한다.
        }

        if (strLength>maxLength) {
            break; // 제한 문자수를 넘길경우.
        } else {
            newStr = newStr + nv;
        }
    }

    if(typeof object == "object"){
        object.value = newStr;
    } else {
        return newStr;
    }
}

function valueFormatter(value, length){
    if(value.length <= length){
        return value;
    } else {
        return value.substring(0, length) + "...";
    }
}

/*str에서 해당 parameter 값 가져오기 ex) page=5&val=5 */
function findGetParameter(str,parameterName) {
    var result = null,
        tmp = [];
    var items = str.split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}
/*뒤로가기*/

/*목록(뒤로가기)*/
function listBack(obj,str){
    var addMode = "";
    if(typeof str != "undefined"){
        location.href = str;
    }
    var chk = "default";
    var isMobile = {
        Android: function () {return navigator.userAgent.match(/Android/i) == null ? false : true;},
        BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i) == null ? false : true;},
        IOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i) == null ? false : true;},
        Opera: function () {return navigator.userAgent.match(/Opera Mini/i) == null ? false : true;},
        Windows: function () {return navigator.userAgent.match(/IEMobile/i) == null ? false : true;},
        any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.IOS() || isMobile.Opera() || isMobile.Windows());}
    };
    if(isMobile.any()){
        chk = "mobile";
        if(isMobile.Android()){
        }else if(isMobile.IOS()){
        }else if(isMobile.BlackBerry()){
        }else if(isMobile.Opera()){
        }else if(isMobile.Windows()){
        }
    }

    var str_hash = document.location.hash.replace("#","");
    if (chk == "mobile" && str_hash.indexOf('nMenuSw') != -1 && sessionStorage.getItem("last-url").indexOf('nMenuSw') == -1) {
        addMode = "#nMenuSw";
    } else if (chk == "mobile" && str_hash.indexOf('nMenuVw') != -1 && sessionStorage.getItem("last-url").indexOf('nMenuVw') == -1) {
        addMode = "#nMenuVw";
    }

    if(sessionStorage.getItem("state") == "view"){
        $(obj).attr("onclick","listBack(this,'"+sessionStorage.getItem("last-url")+ addMode+"')");
        if (addMode == "" && str_hash.indexOf('nMenuSw') == -1 && str_hash.indexOf('nMenuVw') == -1) {
            $(obj).attr("onclick",$(obj).attr("onclick").replace("#nMenuVw",""));
            $(obj).attr("onclick",$(obj).attr("onclick").replace("#nMenuSw",""));
        }
    }
}



/*글자 조회*/
function setkeyup(){
    $('.remaining').each(function() {
        var $maximumCount = $(this).attr("max") * 1;
        var $input = $(this);
        var update = function() {
            var now = $maximumCount - $input.val().length;
            // 사용자가 입력한 값이 제한 값을 초과하는지를 검사한다.
            if (now < 0) {
                var str = $input.val();
                popAlertLayer('글자 입력수가 초과하였습니다.');
                $input.val(str.substr(0, $maximumCount));
                now = 0;
            }
        }
        $input.bind('input keyup paste', function() {
            setTimeout(update, 0)
        });
        update();
    })
}

/* ul header*/
function setHeaderUse(){
    var firstName = $(".leftMenuOn").text().trim();
    var secondName = $(".leftSubMenuOn").text().trim();
    var thirdName = $("#tabGrp .tabOn").text().trim() || $(".conTitle.tabOn").text().trim() || $(".conTitle").text().trim();

    var firstUrl = $(".leftMenuOn").next(".leftSubMenu").children("a").attr("onclick");
    var secondUrl = $(".leftSubMenuOn a").attr("onclick");
    var thirdUrl = "window.location.reload()";

    var sVal = $("#leftSubMenuNum").val();

    $("div.locate > ul").html("");
    $("div.locate > ul").append("<li class='iconHome'>&nbsp;</li>");
    var hasPrevious = false;
    if (firstName) {
        hasPrevious = true;
    $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + firstUrl + "\">" + firstName + "</li>");
    }
    if (sVal) {
        if (hasPrevious) {
        $("div.locate > ul").append("<li><img src='/resources/image/gts.gif'>&nbsp;</li>");
        }
        hasPrevious = true;
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + secondUrl + "\">" + secondName + "</li>");
    }

    if (thirdName) {
        if (hasPrevious) {
        $("div.locate > ul").append("<li><img src='/resources/image/gts.gif'>&nbsp;</li>");
        }
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + thirdUrl + "\">" + thirdName + "</li>");
    }
}

function resizeJqGridWidth(grid_id, div_id){
    $(window).bind("resize", function() { // 그리드의 width 초기화
        var resizeWidth = $("#gridArea").width();

        $("#" + grid_id).setGridWidth(resizeWidth, true);
        $("#" + grid_id).css("width", "100%");
    }).trigger("resize");
}


function keyup(){
    $(".onlynum").keyup(function(e) {
        $(this).val($(this).val().replace(/[^\d]+/g, ''));
    });
}


function dateString(dayVal){
    var str = dayVal.getFullYear()+"-";
    var month = dayVal.getMonth()+1;
    var day = dayVal.getDate();
    if(month <10){        str += "0";        }
    str += (dayVal.getMonth()+1)+"-";
    if(day <10){        str += "0";        }
    str += dayVal.getDate();

    return str;
}

function dateTimeString(dayVal){
    var str = dayVal.getFullYear()+"-";
    var month = dayVal.getMonth()+1;
    var day = dayVal.getDate();
    if(month <10){        str += "0";        }
    str += (dayVal.getMonth()+1)+"-";
    if(day <10){        str += "0";        }
    str += dayVal.getDate();

    var hour = dayVal.getHours();
    str += " ";
    if(hour <10){        str += "0";        }
    str += hour+":";

    var minute = dayVal.getMinutes();
    if(minute <10){        str += "0";        }
    str += minute;

    return str;
}


function is_number(x)

{

    var reg = /^\d+$/;

    return reg.test(x);

}

function validatePassword(value) {
    var pwdRegular = /^[a-z|0-9]{10,16}$/gi;
    return pwdRegular.test(value);
}

function removeHyphen(str) {
    if (!str) {
        return str;
    }
    return str.replace(/-/g, "");
}

function validateId(str) {
    if (!str || str.length <=0) {
        return false;
    }
    var regStr = /^[a-z]+[a-z|0-9]{5,14}$/gi;
    return regStr.test(str);
}

function validateName(str) {
    if (!str || str.length <= 0) {
        return false;
    }
    var regStr = /^[가-힣a-zA-Z]{2,10}$/gi;
    return regStr.test(str);
}

function validatePhoneNumber(str) {
    if (!str) {
        return false;
    }

    // TODO : 전화번호 구조 충족 여부 추가 구현 필요
    str = str.split('-').join('');
    if (str.length <= 0 || str.length > 11) {
        return false;
    }
    var regStr = /^[0-9]*$/;
    return regStr.test(str);
}

function phoneNumberFormatter(num) {
    if (!num) {
        return num;
    }
    return num.replace(/-/g, "").replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
}

function validateBizNo(str) {
    if (!str) {
        return false;
    }

    // TODO : 사업자번호 구조 충족 여부 추가 구현 필요
    str = str.split('-').join('');
    if (str.length != 10) {
        return false;
    }

    var regStr = /^[0-9]*$/;
    return regStr.test(str);
}

function bizNoFormatter(num) {
    if (!num) {
        return num;
    }
    return num.replace(/-/g, "").replace(/(\d{3})(\d{2})(\d{5})/g, "$1-$2-$3");
}

function validateEmail(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var regStr = /^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;
    return regStr.test(str);
}

function validateIpAddress(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var expUrl = /^(1|2)?\d?\d([.](1|2)?\d?\d){3}$/;
    return expUrl.test(str);
}


function checkUndefined(str){
    if (typeof str == "undefined") {
        return "";
    } else {
        return str;
    }

}


function toMB(str) {
    var num = str / 1024 /1024;
    num = num.toFixed(1);

    return num;
}


/* XSS 값 replace*/
function xssChk(str){
    var text = str;
    var decoded = $('<div/>').html(str).text();

    return decoded;
}

//이미지 사이즈 동적 보정 (4:3비율 유지)
function adjustContentImageSize(imageElements) {
    $(imageElements).each(function(i, item) {
        var height = ($(item).parent().width() * 0.75) + "px";
        $(item).css("height", height);
        $(item).parent().css("height", height);
    });
}

/**
 * Client에서 개인정보 마스킹 처리 코드
 * Server 단 처리로 현재 사용하지 않는 코드이지만 소스 내 호출 코드가 있어,
 * 원형만 유지하고 전달 받은 값을 그대로 return 하도록 함.
 * 또한 Client 단에서 처리 필요 시 사용토록 주석으로 코드 유지함.
 * @param name
 * @returns
 */
//회원 이름 숨김 처리 - 뒤 1자리 (이름 길이의 1/3 영역 숨김)
function maskName(name) {
    /*
    if (name == undefined || name === '') {
        return '';
    }

    var nameLen = name.length;
    var maskLen = Math.round(nameLen / 3);
    var nonMaskStr = name.substr(0, nameLen - maskLen);
    var maskStr = name.substr(nameLen - maskLen, nameLen);

    for (var i = 0; i < maskStr.length; i++) {
        maskStr = maskStr.replace(maskStr.substring(i, i+ 1), "*");
    }

    return nonMaskStr + maskStr;
    */
    return name;
}

function imgToThumbImg(str) {
    if (str == "conImg") {
        if (isDevice == "mobile") {
            $('img[src*="/api/imgUrl"]').each(function() {
                $(this).attr('src',$(this).attr("src").replace(/width=+\S*&/gi, ""));
                $(this).attr('src',$(this).attr("src").replace("/imgUrl?", "/imgUrl?width=300&height=225&"));
            });
        } else {
            $('img[src*="/api/imgUrl"]').each(function() {
                $(this).attr('src',$(this).attr("src").replace(/width=+\S*&/gi, ""));
                $(this).attr('src',$(this).attr("src").replace("/imgUrl?", "/imgUrl?width=240&height=180&"));
            });
        }
    }
}
