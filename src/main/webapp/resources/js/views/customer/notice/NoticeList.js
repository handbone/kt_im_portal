/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var gridState;
var tempState;
var searchField;
var searchString;
var totalPage = 0;

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#", "");
            searchField = checkUndefined(findGetParameter(str_hash, "searchField"));
            searchString = checkUndefined(findGetParameter(str_hash, "searchString"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            jQuery("#jqgridData").trigger("reloadGrid");
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    top.document.title = getMessage("menu.customer.notice") + " : GiGA Live On";

    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#", "");
        searchField = checkUndefined(findGetParameter(str_hash, "searchField"));
        searchString = checkUndefined(findGetParameter(str_hash, "searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    noticeList();

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });

    $('.remaining').each(function() {
        var $maximumCount = $(this).attr("max") * 1;
        var $input = $(this);
        var update = function() {
            var now = $maximumCount - $input.val().length;
            // 사용자가 입력한 값이 제한 값을 초과하는지를 검사한다.
            if (now < 0) {
                var str = $input.val();
                alert(getMessage("common.input.max.limit.first.msg") + ' ' + $maximumCount + getMessage("common.input.max.limit.second.msg"));
                $input.val(str.substr(0, $maximumCount));
                $input.val($input.val().replace(/[\<>&\"']/gi, ''));
                now = 0;
            }
        }
        $input.bind('input keyup paste', function() {
            setTimeout(update, 0)
        });
        update();
    });

    $(".ui-pg-input").keydown(function(e) {

        var keyCodeNo = 0;
        var currentPage = Number($(".ui-pg-input").val());
        var totalPage = Number($("#sp_1_pageDiv").text());

        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            if (currentPage > totalPage) {
                alert(getMessage("common.errpage.msg"));
            }

            keywordSearch();
        }
    });
});

noticeList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("table.title"),
        getMessage("table.sbst"),
        getMessage("table.retvnum"),
        getMessage("table.file"),
        getMessage("table.regdate"),
        "noticePrefRank",
        "noticeSeq"
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/notice"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.noticeList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:50},
            {name:"noticeTitle", index: "NOTICE_TITLE", align:"left", formatter:pointercursor, width:400},
            {name:"noticeSbst", index: "NOTICE_SBST", hidden:true},
            {name:"retvNum", index:"RETV_NUM", align:"center", hidden:true},
            {name:"fileExist", index:"FILE", align:"center", hidden:true},
            {name:"regDt", index:"REG_DT", align:"center", width:140},
            {name:"noticePrefRank", index:"NOTICE_PREF_RANK", hidden:true},
            {name:"noticeSeq", index:"NOTICE_SEQ", hidden:true}
        ],
        caption: "",
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "NOTICE_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: false,
        beforeRequest:function(){
            tempState = gridState;
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#", "");
            var page = parseInt(findGetParameter(str_hash, "page"));
            searchField = checkUndefined(findGetParameter(str_hash, "searchField"));
            searchString = checkUndefined(findGetParameter(str_hash, "searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "SEARCH") {
                    myPostData.page = 1;
                } else {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash, "searchField"));
                searchString = checkUndefined(findGetParameter(str_hash, "searchString"));
                myPostData._search = true;
                myPostData.searchField = searchField;
                myPostData.searchString = searchString;
                myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true;
                myPostData.searchField = searchField;
                myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString;
                delete myPostData.searchField;
                myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData, page: myPostData.page });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            //session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            //Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page=" + $(this).context.p.page;
            if($(this).context.p.postData._search && $("#target").val() != null){
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            $(this).find("td").each(function(){
                if ($(this).index() == 0) { // 번호
                    if ($(this).parent("tr").find("td:eq(6)").text() != "" && $(this).parent("tr").find("td:eq(6)").text() != "0") {
                        $(this).html("<img src='" + makeAPIUrl("/resources/image/icon_important.png") + "' style='width: 13px;vertical-align: middle;' />");
                    }
                } else if ($(this).index() == 1) { // 제목
                    var seqStr = $(this).parent("tr").find("td:eq(7)").text();
                    var hrefHtml = "";
                    if ($(this).parent("tr").find("td:eq(6)").text() != "" && $(this).parent("tr").find("td:eq(6)").text() != "0") {
                        hrefHtml += "<a href='javascript:moveToNoticeDetail(" + seqStr + ")';>" + "[" + getMessage("notice.important") + "] " + $(this).text() + "</a>";
                    } else {
                        hrefHtml += "<a href='javascript:moveToNoticeDetail(" + seqStr + ")';>" + $(this).text() + "</a>";
                    }
                    $(this).html(hrefHtml);

                    // 제목 a 태그에 mouseover 시에만 색상 변경되도록 함.
                    $(this).children("a").mouseover(function(){
                        $(this).css("color", "#18e4c7");
                        $(this).parents("td").css("color", "#18e4c7");
                    });
                    $(this).children("a").mouseout(function(){
                        $(this).css("color", "#fff");
                        $(this).parents("td").css("color", "#fff");
                    });
                } else if ($(this).index() == 4) { // FILE
                    if ($(this).text() == "true") {
                        $(this).html("<img src='" + makeAPIUrl("/resources/image/icon_file.png") + "' style='width: 13px; vertical-align: middle;' />");
                    } else {
                        $(this).html("");
                    }
                }
            });

            //포인터
            $(this).find(".pointer").parent("td").addClass("pointer");

            $("#jqgridData").css("width", "100%");

            resizeJqGridWidth("jqgridData", "gridArea");
        },
        beforeSelectRow: function(rowId, event) {
            // 공지사항 cell 선택 시 highlight 선택되지 않도록 함.
            return false;
        },
        afterInsertRow: function (rowid, rowData, rowelem) {
            if (rowData.noticePrefRank != '0') {
                $("#jqgridData").jqGrid('setRowData', rowid, false, 'noticeImptRowBold');
            }
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */

        onCellSelect: function(rowId, columnId, cellValue, event){
            /*
            // 제목 셀 클릭시
            if (columnId == 1) {
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/notice/" + list.noticeSeq);
            }
            */
        }

    });
    jQuery("#jqgridData").jqGrid('navGrid', '#pageDiv', {del:false, add:false, edit:false, search:false});

    $(".ui-jqgrid-htable").append("<caption style='display: none'>공지사항</caption>");

    // 목록 타이틀 (번호, 제목, 등록일) tabindex 적용
    $("#jqgh_jqgridData_num").attr('tabindex','0');
    $("#jqgh_jqgridData_noticeTitle").attr('tabindex','0');
    $("#jqgh_jqgridData_regDt").attr('tabindex','0');
}

moveToNoticeDetail = function(seq) {
    sessionStorage.setItem("last-url", location);
    sessionStorage.setItem("state", "view");
    pageMove("/notice/" + seq);
}

function keywordSearch() {
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

searchFieldSet = function() {
    if ($("#target > option").length == 0) {
        var searchHtml = "";
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i = 0; i < colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name){
            case "noticeTitle":
                break;
            case "noticeSbst":
                break;
            default:
                continue;
            }
            searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
        }
        $("#target").html(searchHtml);
    }
    $("#target").val(searchField);
}
