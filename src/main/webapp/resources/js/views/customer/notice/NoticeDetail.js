/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    top.document.title = getMessage("menu.customer.notice") + " : GiGA Live On";
    noticeInfo();

    listBack($(".btnList"));
});

function noticeInfo(){
    formData("NoneForm", "noticeSeq", $("#noticeSeq").val());
    callByGet("/api/notice", "noticeInfoSuccess", "NoneForm", "noticeInfoFail");
}

function noticeInfoSuccess(data){
    formDataDelete("NoneForm", "noticeSeq");
    if(data.resultCode == "1000"){
        var noticeDetail = data.result.noticeDetail;
        $("#noticeTitle").html(noticeDetail.noticeTitle);
        $("#noticeViewInfo").html("<table><caption style='display: none'>" + getMessage("notice.detail.table.caption") + "</caption><tr><td class='writer'><span class='viewLarge'>" + getMessage("table.reger") + " : </span>" + noticeDetail.cretrNm + "</td><td class='counter'><span class='viewLarge'>" + getMessage("table.retvnum") + " : </span>"  + noticeDetail.retvNum + "</td><td class='wdate'><span class='viewLarge'>" + getMessage("common.createDate") + " : </span>" + noticeDetail.regDt + "</td></tr></table>" );

        if (data.result.fileList.length == 0) {
            $("#noticeViewDetail").html(xssChk(noticeDetail.noticeSbst));
            $("#noticeViewDetail").html($("#noticeViewDetail").text());
        } else {
            var fileListHtml = "<div class='attatchFileGrp'><table><tr><td class='attatch'><strong>" + getMessage("common.attachfile") + " : </strong></td><td>";

            $(data.result.fileList).each(function(i,item) {
                fileListHtml += "<p><span style='float:left;' >" + item.fileName + "</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.fileSeq + "' class='fontwhite btnDownload'> [" + getMessage("common.download") + "]</a></p>";
            });
            fileListHtml += "</td></tr></table></div>";
            $("#noticeViewDetail").html(xssChk(strConv(noticeDetail.noticeSbst)) + fileListHtml);
        }
    } else {
        alert(getMessage("common.bad.request.msg"));
        pageMove("/notice");
    }
}

function noticeInfoFail(data) {
    alert(getMessage("common.bad.request.msg"));
    pageMove("/notice");
}