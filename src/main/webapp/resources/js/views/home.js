/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

//slide
var slideIndex = 1;
var slideMaxIndex = 3;
var slidePlay = false;
var filter = "win16|win32|win64|mac";

//slide
var slideIndexSm = 1;
var slideMaxIndexSm = 3;
var slidePlaySm = false;

var isMouseDown = false;

$(window).resize(function() {
    if (isDevice != preDevice) {
        preDevice = isDevice
    }

    //현재 페이지 내 이미지를 썸네일 이미지로 교체
    imgToThumbImg("conImg");
    adjustContentImageSize($(".conImg img"));
})

$(document).ready(function(){
    top.document.title = getMessage("menu.main.title");

    recommendContentsList();

    jQuery('#parallax .parallax-layer').parallax({
        mouseport: jQuery('#parallax')
    });

    if(navigator.platform){
        if(0 > filter.indexOf(navigator.platform.toLowerCase())){
            slidePlay = false;
            slidePlaySm = true;
            $('#dotm1').addClass('active');
            showSlidesSm(slideIndexSm);
        } else {
            slidePlay = true;
            slidePlaySm = false;
            $('#dot1').addClass('active');
            $('#dot1').attr("title", "1" + getMessage("common.banner.select.title"));
            $('#dot2').attr("title", "2" + getMessage("common.banner.not.select.title"));
            $('#dot3').attr("title", "3" + getMessage("common.banner.not.select.title"));
            $('#dot4').attr("title", "4" + getMessage("common.banner.not.select.title"));
        }
    }
});

/* 배너의 next, prev 버튼에 대해 mousedown일 경우 focus를 그리지 않도록 하기 위해 추가 - start */
$(document).on("mousedown", function(e){
    isMouseDown = true;
});

$(document).on("keydown", function(e){
    isMouseDown = false;
});
/* 배너의 next, prev 버튼에 대해 mousedown일 경우 focus를 그리지 않도록 하기 위해 추가 - end */

recommendContentsList = function(){
    callByGet("/api/recommendContents", "recommendContentsListSuccess", "NoneForm");
}

recommendContentsListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var recommendContentHtml = "";

        $(data.result.recommendContentList).each(function(i, item) {
            recommendContentHtml += "<li>";
            recommendContentHtml += "<div class=\"conBox\">";
            recommendContentHtml += "<a href='javascript:;' onclick='pageMove(\"/contents/" + item.contsSeq + "\")' style=\"text-decoration:none;\" \>";

            recommendContentHtml += "<div class=\"conAll\">";
            recommendContentHtml += "<div class=\"conImg-wrapper\">";
            recommendContentHtml += "<div class=\"conImg\"><img src=\"" + makeAPIUrl(item.filePath, "img") + "\" alt=\"\"\></div>";
            recommendContentHtml += "</div>";
            recommendContentHtml += "<div class=\"conDesc\"><p class='contTHeight'><span class=\"name\">" + item.contsTitle + "</span></p>";
            recommendContentHtml += "<p class='contDHeight'><span class=\"des\">" + item.contsSubTitle + "</span></p>";
//            recommendContentHtml += "<p class='contDHeight'><span class=\"cat\">" + item.firstCtgNm + "(" + item.secondCtgNm + ") / "+ item.genreNm + " / " + item.maxAvlNop + "명</span></p></div>";

            var str = item.firstCtgNm.split("_");
            var remainTxt = item.firstCtgNm.replace(str[0]+"_","");
            recommendContentHtml += "<p class='contDHeight'><span class=\"cat\">";
            recommendContentHtml += str[0];
            if (remainTxt != "") {
                recommendContentHtml += "(";
                recommendContentHtml += remainTxt;
                recommendContentHtml += ")";
            }
            recommendContentHtml += " / "+ item.genreNm + " / " + item.maxAvlNop + "명";
            recommendContentHtml += "</span></p></div>";
            recommendContentHtml += "</div>";
            recommendContentHtml += "</a>";
            recommendContentHtml += "</div>";
            recommendContentHtml += "</li>";
        });

        $("#selectRecommendContents").html(recommendContentHtml);

        sessionStorage.setItem("last-url", location);
        sessionStorage.setItem("state", "view");

        // 현재 페이지 내 이미지를 썸네일 이미지로 교체
        imgToThumbImg("conImg");
        adjustContentImageSize($(".conImg img"));
    } else if(data.resultCode == "1010"){
        $("#selectRecommendContents").html("<tr><td colspan=3 align=center height=80><spring:message code='common.nodata.msg' /></td><tr>");
    }
}

/** Banner S **/

function plusSlides(n, e) {
    if (isMouseDown) {
        $(e).blur();
    }

    showSlidesSm(slideIndexSm += n);
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
    $('.dotGrp .dotm.active, .dotGrp .dot.active').blur();
}

function showSlides(n) {
    var i;
    var slides1 = document.getElementsByClassName("mySlides");
    var slides2 = document.getElementsByClassName("mySlides2");
    var slides3 = document.getElementsByClassName("mySlides3");
    var slides4 = document.getElementsByClassName("mySlides4");
    var dots = document.getElementsByClassName("dot");

    if (n > slides1.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides1.length}
    for (i = 0; i < slides1.length; i++) {
        slides1[i].style.display = "none";
        slides2[i].style.display = "none";
        slides3[i].style.display = "none";
        slides4[i].style.display = "none";
    }

    for (i = 0; i < dots.length; i++) {
        $("#" + $(".active").attr("id")).attr("title", (i + 1) + getMessage("common.banner.not.select.title"));
        dots[i].className = dots[i].className.replace(" active", "");
    }

    slides1[slideIndex-1].style.display = "block";
    slides2[slideIndex-1].style.display = "block";
    slides3[slideIndex-1].style.display = "block";
    slides4[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";

    $("#" + $(".active").attr("id")).attr("title", n + getMessage("common.banner.select.title"));
}

function playSlides(){
    document.getElementById("playBtn").classList.toggle("start");
    document.getElementById("playBtn").classList.toggle("stop");
    slidePlay = !slidePlay;
}



function plusSlidesSm(n, e) {
    if (isMouseDown) {
        $(e).blur();
    }

    showSlides(slideIndex += n);
    showSlidesSm(slideIndexSm += n);
};

function currentSlideSm(n) {
  showSlidesSm( slideIndexSm = n);
  $('.dotGrp .dotm.active, .dotGrp .dot.active').blur();
};

function showSlidesSm(n) {
  var i;
  var slidesSm = document.getElementsByClassName("myslidesSm");
  var dots = document.getElementsByClassName("dotm");
  if (n > slidesSm.length) { slideIndexSm = 1}
  if (n < 1) { slideIndexSm = slidesSm.length}
  for (i = 0; i < slidesSm.length; i++) {
      slidesSm[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
      $("#" + $(".dotm.active").attr("id")).attr("title", (i + 1) + getMessage("common.banner.not.select.title"));
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slidesSm[slideIndexSm-1].style.display = "block";
  dots[slideIndexSm-1].className += " active";
  $("#" + $(".dotm.active").attr("id")).attr("title", n + getMessage("common.banner.select.title"));
};

function playSlidesSm(e){
    document.getElementById("playBtnSm").classList.toggle("start");
    document.getElementById("playBtnSm").classList.toggle("stop");
    slidePlaySm = !slidePlaySm;
    $(e).toggleClass("start");
    $(e).toggleClass("stop");
};

window.onload = function(){
    /*
    setInterval(function(){
        if(slidePlay){
            if(slideIndex >= slideMaxIndex){
                slideIndex = 1;
            } else {
                slideIndex++;
            }
            showSlides(slideIndex);
        } else if (slidePlaySm){
            if(slideIndexSm >= slideMaxIndexSm){
                 slideIndexSm = 1;
            } else {
                 slideIndexSm++;
            }
            showSlidesSm(slideIndexSm);
        }
    }, 6000);
    */
    setInterval(function(){
        if(slidePlay){
            if(slideIndex >= slideMaxIndex){
                slideIndex = 1;
            } else {
                slideIndex++;
            }
            showSlides(slideIndex);

            if(slideIndexSm >= slideMaxIndexSm){
                slideIndexSm = 1;
            } else {
                slideIndexSm++;
            }
            showSlidesSm(slideIndexSm);
        } else if (slidePlaySm){
            if(slideIndex >= slideMaxIndex){
                slideIndex = 1;
            } else {
                slideIndex++;
            }
            showSlides(slideIndex);

            if(slideIndexSm >= slideMaxIndexSm){
                 slideIndexSm = 1;
            } else {
                 slideIndexSm++;
            }
            showSlidesSm(slideIndexSm);
        }
    }, 6000);
//슬라이드 버튼 숨기기 (슬라이드가 한개일경우)
//    if(slideMaxIndex <= 1) {
//        $(".next").hide();
//        $(".prev").hide();
//        $(".dotm").hide();
//        $(".dot").hide();
//    }
}

/** Banner E **/



