/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var totalPage = 1;
var cntGenre = 0;
var page = 1;
var keyword = "";
var order = "AMD_DT";
var selMenu = 0;
var againAnimate;
var isMouseDown = false;
var beforeHash = "";

$(window).on("scroll", function() {
    var docHeight = $(document).height();
    var winScrolled = $(window).height() + $(window).scrollTop(); // Sum never quite reaches
    if ((docHeight - winScrolled) < 1) {
        if(totalPage > page) {
            contentsDisplayListMore();
        }
    }
});

$(window).resize(function(){
    real_mobile_check();
    $("a[title='선택됨']").parent().addClass("selected");
    if ($("div.selected").index() == -1) {
        $("span.selected").parent().parent("div").attr("class", "selected");
    } else {
        setTimeout(function(){ $("span.selected").parent().parent("div").attr("class", "selected"); }, 1);
    }

    if (isDevice != preDevice) {
        preDevice = isDevice;
    }

    //현재 페이지 내 이미지를 썸네일 이미지로 교체
    imgToThumbImg("conImg");
    adjustContentImageSizeType2($(".conImg img"));

    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function(){
        $("a[title='선택됨']").parent().addClass("selected");
        $("span.selected").parent().parent("div").attr("class", "selected");

        $(".selected .menuTab").find("span").stop(true,true);
        $(".selected .menuTab").find("span").css({"text-overflow":"ellipsis","overflow": "hidden","width": "100%","left":"0px"});
        moveMenu();

        $(".menuTab").off("click");
        $(".menuTab").bind("click",function(){
            if (selMenu == $(this).parents(".slick-slide").index()) {
                return;
            }
            selMenu = $(this).parents(".slick-slide").index();
            $(".menuTab").find("span").stop(true,true);
            $(".menuTab").find("span").css({"text-overflow":"ellipsis","overflow": "hidden","width": "100%","left":"0px"});
            moveMenu();
        });
    }, 250);
});

$(document).ready(function(){
    top.document.title = "";
    ctgInfo();

    contentsGenreList();
    real_mobile_check();
    $("#orderSelbox").change(function(){
        // select box(최신순/이름순) 선택 후 마우스 클릭일 경우 focus 표시 삭제
        if (isMouseDown) {
            $("#orderSelbox").blur();
        }

        if ($("#orderSelbox option:selected").val() == "CONTS_TITLE") {
            order = "CONTS_TITLE";
        } else if ($("#orderSelbox option:selected").val() == "AMD_DT") {
            order = "AMD_DT";
        }
        page = 1;
        contentsDisplayList();
    });

    $(window).on('hashchange', function () {
        var chk = "default";
        var isMobile = {
            Android: function () {return navigator.userAgent.match(/Android/i) == null ? false : true;},
            BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i) == null ? false : true;},
            IOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i) == null ? false : true;},
            Opera: function () {return navigator.userAgent.match(/Opera Mini/i) == null ? false : true;},
            Windows: function () {return navigator.userAgent.match(/IEMobile/i) == null ? false : true;},
            any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.IOS() || isMobile.Opera() || isMobile.Windows());}
        };
        if(isMobile.any()){
            chk = "mobile";
            if(isMobile.Android()){
            }else if(isMobile.IOS()){
            }else if(isMobile.BlackBerry()){
            }else if(isMobile.Opera()){
            }else if(isMobile.Windows()){
            }
        }

        var str_hash = document.location.hash.replace("#","");

        if (chk == "mobile" && str_hash.indexOf('nMenuSw') != -1) {
            location.reload();
        } else if (chk == "mobile" && str_hash.indexOf('nMenuVw') != -1 && beforeHash.indexOf('nMenuSw') != -1) {
            location.reload();
        }

        real_mobile_check();
    });
});

/* slick-slide 관련으로 mousedown일 경우 tab focus를 그리지 않도록 하기 위해 추가 - start */
$(document).on("mousedown", function(e){
    isMouseDown = true;
});

$(document).on("keydown", function(e){
    isMouseDown = false;
});
/* slick-slide 관련으로 mousedown일 경우 tab focus를 그리지 않도록 하기 위해 추가 - end */

ctgInfo = function() {
    var firstCtgID = window.location.pathname.replace(makeAPIUrl("/display/"), "");
    callByGet("/api/categoryName?firstCtgID=" + firstCtgID, "ctgInfoSuccess", "NoneForm");
}

ctgInfoSuccess = function(data) {
    if(data.resultCode == "1000"){
        top.document.title = data.result.ctgInfo.firstCtgNm + " : GiGA Live On";
    } else {
        top.document.title = getMessage("menu.contents.title") + " : GiGA Live On";
    }
}

contentsDisplayList = function(){
    var firstCtgID = window.location.pathname.replace(makeAPIUrl("/display/"), "");

    var str_hash = document.location.hash.replace("#","");

    var chk = "default";
    var isMobile = {
        Android: function () {return navigator.userAgent.match(/Android/i) == null ? false : true;},
        BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i) == null ? false : true;},
        IOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i) == null ? false : true;},
        Opera: function () {return navigator.userAgent.match(/Opera Mini/i) == null ? false : true;},
        Windows: function () {return navigator.userAgent.match(/IEMobile/i) == null ? false : true;},
        any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.IOS() || isMobile.Opera() || isMobile.Windows());}
    };
    if(isMobile.any()){
        chk = "mobile";
        if(isMobile.Android()){
        }else if(isMobile.IOS()){
        }else if(isMobile.BlackBerry()){
        }else if(isMobile.Opera()){
        }else if(isMobile.Windows()){
        }
    }

    if (chk == "mobile" && str_hash.indexOf('nMenuSw') != -1) {
        formData("NoneForm", "mode", "o_1");
    }


    formData("NoneForm", "firstCtgID", firstCtgID);
    formData("NoneForm", "searchString", keyword);
    formData("NoneForm", "sidx", order);
    callByGet("/api/contentsDisplay", "contentsDisplayListSuccess", "NoneForm");
}

function contentsGenreList() {
    var firstCtgID = window.location.pathname.replace(makeAPIUrl("/display/"), "");

    if(firstCtgID == 'G') {
        $("#contentListTitle").html(getMessage("menu.contents.game"));
    } else if(firstCtgID == 'V') {
        $("#contentListTitle").html(getMessage("menu.contents.movie"));
    } else if(firstCtgID == 'A') {
        $("#contentListTitle").html(getMessage("menu.contents.music"));
    } else if(firstCtgID == 'S') {
        $("#contentListTitle").html(getMessage("menu.contents.sport"));
    }
    $.ajax({
        dataType : 'json',
        data : {"firstCtgID" : firstCtgID},
        url : makeAPIUrl('/api/genreList'),
        success: function(data) {
            if(data.resultCode == "1000"){
                var contentHtml = "";

                // slick-slide 탭 이동에 따른 focus 적용을 위해 onfocus 추가
                contentHtml += "<a href='javascript:;' style='position:relative' class='menuTab' onclick='keywordSet(\"\", " + cntGenre + ")' onfocus='focusSet(this,\"\"," + cntGenre + ")' id='contentGenre'><span class='selected'>" + getMessage("common.button.all") + "</span></a>";

                $(data.result.contentsGenreList).each(function(i, item) {
                    cntGenre = i + 1;
                 // slick-slide 탭 이동에 따른 focus 적용을 위해 onfocus 추가
                    contentHtml += "<a href='javascript:;' style='position:relative' class='menuTab' onclick='keywordSet(\"" + item.genreNm + "\", " + cntGenre + ")' onfocus='focusSet(this,\"" + item.genreNm + "\", " + cntGenre + ")' ondblclick='scrollCategory(this)' id='contentGenre'><span>" + item.genreNm + "</span></a>";
                });

                $("#contentsGenreList").append(contentHtml);

                var orderHtml = "";
                orderHtml += "<option value=\"CONTS_TITLE\">" + getMessage("display.order.title") + "</option>";
                orderHtml += "<option value=\"AMD_DT\" selected>" + getMessage("display.order.regist") + "</option>";
                $("#orderSelbox").html(orderHtml);

                if (document.location.hash) {
                    var str_hash = document.location.hash.replace("#", "");
                    order = findGetParameter(str_hash, "sidx");
                    $("#orderSelbox > option[value=" + order + "]").attr("selected", "true");
                }

                /* 콘텐츠 목록 장르 리스트 슬라이더 */
                $(".regular").slick({
                    dots: false,
                    infinite: false,
                    slidesToShow: 9,
                    slidesToScroll: 9,
                    responsive: [
                        {
                            breakpoint: 910,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 6
                            }
                        },
                        {
                            breakpoint: 667,
                            settings: {
                                slidesToShow: 5,
                                slidesToScroll: 5
                            }
                        },
                        {
                            breakpoint: 490,
                            settings: {
                                slidesToShow: 3.5,
                                slidesToScroll: 3
                            }
                        }
                    ]
                });
                $(".menuTab").bind("click",function(){
                    if (selMenu == $(this).parents(".slick-slide").index()) {
                        return;
                    }
                    selMenu = $(this).parents(".slick-slide").index();
                    $(".menuTab").find("span").stop(true,true);
                    $(".menuTab").find("span").css({"text-overflow":"ellipsis","overflow": "hidden","width": "100%","left":"0px"});
                    moveMenu();
                });
                $("span.selected").parent().parent("div").attr("class", "selected");
                $("span.selected").parent().parent("div").children().attr("title", "선택됨");
                $(".slick-arrow").mouseup(function() {
                    this.blur();
                });
                contentsDisplayList();
            } else {
                var orderHtml = "";
                orderHtml += "<option value=\"CONTS_TITLE\">" + getMessage("display.order.title") + "</option>";
                orderHtml += "<option value=\"AMD_DT\" selected>" + getMessage("display.order.regist") + "</option>";
                $("#orderSelbox").html(orderHtml);

                // slick-slide 탭 이동에 따른 focus 적용을 위해 onfocus 추가
                $("#contentsGenreList").append("<a href='javascript:;' class='menuTab' style='position:relative' onclick='keywordSet(\"\", " + cntGenre + ")' onfocus='focusSet(this,\"\"," + cntGenre + ")' id='contentGenre' title='" + "전체" + "'><span class='selected'>" + getMessage("common.button.all") + "</span></a>");
                $(".regular").slick({
                    dots: false,
                    infinite: false,
                    slidesToShow: 9,
                    slidesToScroll: 9,
                    responsive: [
                        {
                            breakpoint: 910,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 6
                            }
                        },
                        {
                            breakpoint: 667,
                            settings: {
                                slidesToShow: 5,
                                slidesToScroll: 5
                            }
                        },
                        {
                            breakpoint: 490,
                            settings: {
                                slidesToShow: 3.5,
                                slidesToScroll: 3
                            }
                        }
                    ]
                });
                $(".menuTab").bind("click",function(){
                    if (selMenu == $(this).parents(".slick-slide").index()) {
                        return;
                    }
                    selMenu = $(this).parents(".slick-slide").index();
                    $(".menuTab").find("span").stop(true,true);
                    $(".menuTab").find("span").css({"text-overflow":"ellipsis","overflow": "hidden","width": "100%","left":"0px"});
                    moveMenu();
                });
                $(window).resize();
                $(".slick-arrow").mouseup(function() {
                    this.blur();
                });
                contentsDisplayList();
            }
       },error:function(e){
           if(data.resultCode == "1010"){
               $("#contentsGenreList").html("<div style='width:100%; min-height:30px; text-align:center; color: #ff0000; font-size: 16px;'>" + getMessage("common.nodata.msg") + "</div>");
           };
       }
    });
}

contentsDisplayListSuccess = function(data){
    $(".slick-slide .slick-active .selected a").blur();


    formDataDelete("NoneForm", "firstCtgID");
    formDataDelete("NoneForm", "searchString");
    formDataDelete("NoneForm", "sidx");

    if(data.resultCode == "1000"){
        var contentHtml = "";

        $("#contentCount").html("TOTAL : " + data.result.totalCount);

        $(data.result.contentDisplayList).each(function(i, item) {
            contentHtml += "<li>";
            contentHtml += "<div class='conBox'>";
            contentHtml += "<a href='javascript:;' onclick='pageMove(\"/contents/" + item.contsSeq + "\")' style=\"text-decoration:none;\" \>";
            contentHtml += "<div class='conAll'>";
            contentHtml += "<div class=\"conImg-wrapper\">";
            contentHtml += "<div class='conImg'><img src='" + makeAPIUrl(item.filePath, "img") + "' alt=''></div>";
            contentHtml += "</div>";
            contentHtml += "<div class=\"conDesc\"><p class='contTHeight'><span class=\"name\">" + item.contsTitle + "</span></p>";
            contentHtml += "<p class='contDHeight'><span class=\"des\">" + item.contsSubTitle + "</span></p>";
//            contentHtml += "<p class='contDHeight'><span class=\"cat\">" + item.firstCtgNm + "(" + item.secondCtgNm + ") / "+ item.genreNm + " / " + item.maxAvlNop + "명</span></p></div>";
            var str = item.firstCtgNm.split("_");
            var remainTxt = item.firstCtgNm.replace(str[0]+"_","");
            contentHtml += "<p class='contDHeight'><span class=\"cat\">";
            contentHtml += str[0];
            if (remainTxt != "") {
                contentHtml += "(";
                contentHtml += remainTxt;
                contentHtml += ")";
            }

            contentHtml += " / "+ item.genreNm + " / " + item.maxAvlNop + "명";
            contentHtml += "</span></p></div>";
            contentHtml += "</div>";
            contentHtml += "</a>";
            contentHtml += "</div>";
            contentHtml += "</li>";
        });

        totalPage = data.result.totalPage;

        $("#contentsList").html(contentHtml);

        sessionStorage.setItem("last-url", location);
        sessionStorage.setItem("state", "view");

        // 현재 페이지 내 이미지를 썸네일 이미지로 교체
        imgToThumbImg("conImg");
        adjustContentImageSizeType2($(".conImg img"));
    } else if(data.resultCode == "1010"){
        $("#contentCount").html("TOTAL  : 0");
        $("#contentsList").html("<div class='conBox' style='width:100%; text-align:center; color: #ff0000; font-size: 16px; min-height:30px;'>" + getMessage("common.nodata.msg") + "</div>");
    }
}

function contentsDisplayListMore() {
    var mode = "";
    var firstCtgID = window.location.pathname.replace(makeAPIUrl("/display/"), "");
    page++;

    var chk = "default";
    var isMobile = {
        Android: function () {return navigator.userAgent.match(/Android/i) == null ? false : true;},
        BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i) == null ? false : true;},
        IOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i) == null ? false : true;},
        Opera: function () {return navigator.userAgent.match(/Opera Mini/i) == null ? false : true;},
        Windows: function () {return navigator.userAgent.match(/IEMobile/i) == null ? false : true;},
        any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.IOS() || isMobile.Opera() || isMobile.Windows());}
    };
    if(isMobile.any()){
        chk = "mobile";
        if(isMobile.Android()){
        }else if(isMobile.IOS()){
        }else if(isMobile.BlackBerry()){
        }else if(isMobile.Opera()){
        }else if(isMobile.Windows()){
        }
    }

    var str_hash = document.location.hash.replace("#","");
    if (chk == "mobile" && str_hash.indexOf('nMenuSw') != -1) {
        mode = "o_1";
    }

    $.ajax({
        dataType : 'json',
        data : {"page" : page, "firstCtgID" : firstCtgID, "searchString" : keyword, "sidx" : order, "mode" : mode},
        url : makeAPIUrl('/api/contentsDisplay'),
        success: function(data) {
            if(data.resultCode == "1000"){
                var contentHtml = "";

                $(data.result.contentDisplayList).each(function(i, item) {
                    contentHtml += "<li>";
                    contentHtml += "<div class='conBox'>";
                    contentHtml += "<a href='javascript:;' onclick='pageMove(\"/contents/" + item.contsSeq + "\")' style=\"text-decoration:none;\" \>";
                    contentHtml += "<div class='conAll'>";
                    contentHtml += "<div class=\"conImg-wrapper\">";
                    contentHtml += "<div class='conImg'><img src='" + makeAPIUrl(item.filePath, "img") + "' alt=''></div>";
                    contentHtml += "</div>";
                    contentHtml += "<div class=\"conDesc\"><p class='contTHeight'><span class=\"name\">" + item.contsTitle + "</span></p>";
                    contentHtml += "<p class='contDHeight'><span class=\"des\">" + item.contsSubTitle + "</span></p>";
                    var str = item.firstCtgNm.split("_");
                    var remainTxt = item.firstCtgNm.replace(str[0]+"_","");
                    contentHtml += "<p class='contDHeight'><span class=\"cat\">";
                    contentHtml += str[0];
                    if (remainTxt != "") {
                        contentHtml += "(";
                        contentHtml += remainTxt;
                        contentHtml += ")";
                    }
                    contentHtml += " / "+ item.genreNm + " / " + item.maxAvlNop + "명";
                    contentHtml += "</span></p></div>";
                    contentHtml += "</div>";
                    contentHtml += "</a>";
                    contentHtml += "</div>";
                    contentHtml += "</li>";
                });

                $("#contentsList").append(contentHtml);
                $(window).trigger("resize");
            }
       },error:function(e){
           if(data.resultCode == "1010"){
               $("#contentsList").html("<div class='conBox' style='width:100%; text-align:center; color: #ff0000; font-size: 16px; min-height:30px;'>" + getMessage("common.nodata.msg") + "</div>");
           };
       }
    });
}

function keywordSet(key, idx){
    keyword = key;
    page = 1;
    var genreLists = document.getElementById("contentsGenreList");
    var genreList = genreLists.getElementsByTagName("span");

    $.each(genreList, function(index, element) {
        $(element).attr("class", " ");
    });

    $.each(genreList, function(index, element) {
        var genreName = $(element).find("span").text();

        if(index == idx){
            $("div.selected").removeClass("selected");
            $("a").removeAttr("title");
            $(element).parent().parent("div").attr("class", "selected");
            $(element).parent().parent("div").children().attr("title", "선택됨");

            scrollIntoVisibleRectIfNeeded(element, "single");
        }
    });

    contentsDisplayList();
}

function scrollCategory(element) {
    scrollIntoVisibleRectIfNeeded(element, "double");
}

function scrollIntoVisibleRectIfNeeded(element, type) {
    if (window.innerWidth > 490) {
        return;
    }

    var isOutOfRangeToLeft = $(element).parent().offset().left < 0;
    var isOutOfRangeToRight = ($(element).parent().offset().left + $(element).parent().width()) > window.innerWidth;
    if (type === "double") {
        isOutOfRangeToLeft = $(element).parent().offset().left == 0;
        isOutOfRangeToRight = ($(element).parent().offset().left + $(element).parent().width()) == window.innerWidth;
    }
    if (isOutOfRangeToLeft) {
        $(".slick-prev").trigger("click");
    } else if (isOutOfRangeToRight) {
        $(".slick-next").trigger("click");
    }
}

function moveMenu(){
    var divWidthBefore = $(".selected .menuTab").find("span").width();
    $(".selected .menuTab").find("span").css('width','auto');

    var spanWidth = ($(".selected .menuTab").width()-$(".selected .menuTab").find("span").width()) -16;
    $(".selected .menuTab").find("span").width(divWidthBefore);

    clearTimeout(againAnimate);
    if(spanWidth + 16 <0){
        $(".selected .menuTab").find("span").css({"text-overflow":"inherit","overflow": "visible","width": "auto","left":"0px"});
        $(".selected .menuTab").find("span").animate({left: spanWidth},2000);
        againAnimate = setTimeout(function(){
            $(".selected .menuTab").find("span").animate({left:0},0);
            moveMenu();
        },3500);
    } else {
        $(".selected .menuTab").find("span").css({"text-overflow":"ellipsis","overflow": "hidden","width": "100%","left":"0px"});
    }
}

//이미지 사이즈 동적 보정 (4:3비율 유지)
function adjustContentImageSizeType2(imageElements) {
    var setWidth = $(".conImg").width();

    $(imageElements).each(function(i, item) {
        var height = (setWidth * 0.75) + "px";
        $(item).css("height", height);
        $(item).parent().css("height", height);
    });
}

// slick-slide 탭 이동에 따른 focus 및 enter key 이벤트 적용
function focusSet(e, key, idx) {
    $("div.focusTab").children("div").children("a").attr("tabIndex", "0");
    $("div.focusTab").removeClass("focusTab").removeAttr("tabIndex");

    if (isMouseDown) {
        isMouseDown = false;
        $(e).blur();
        return;
    }

    $(e).attr("tabIndex", "-1");
    $(e).parent("div").parent("div").addClass("focusTab").attr("tabIndex", "0").focus();

    $(".focusTab").off("keydown");
    $(".focusTab").bind("keydown",function(e){
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode != 13) {
            return;
        }

        if (selMenu == $(".focusTab").index()) {
            return;
        }
        selMenu = $(".focusTab").index();
        keywordSet(key, idx);

        // 장르명이 길 경우 스크롤되어 보여지는 코드 추가
        $(".menuTab").find("span").stop(true,true);
        $(".menuTab").find("span").css({"text-overflow":"ellipsis","overflow": "hidden","width": "100%","left":"0px"});
        moveMenu();
    });
}

function real_mobile_check(){
    var chk = "default";
    var isMobile = {
        Android: function () {return navigator.userAgent.match(/Android/i) == null ? false : true;},
        BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i) == null ? false : true;},
        IOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i) == null ? false : true;},
        Opera: function () {return navigator.userAgent.match(/Opera Mini/i) == null ? false : true;},
        Windows: function () {return navigator.userAgent.match(/IEMobile/i) == null ? false : true;},
        any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.IOS() || isMobile.Opera() || isMobile.Windows());}
    };
    if(isMobile.any()){
        chk = "mobile";
        if(isMobile.Android()){
        }else if(isMobile.IOS()){
        }else if(isMobile.BlackBerry()){
        }else if(isMobile.Opera()){
        }else if(isMobile.Windows()){
        }
    }

    var str_hash = document.location.hash.replace("#","");
    beforeHash = str_hash;
    if (chk == "mobile" && (str_hash.indexOf('nMenuVw') != -1 || str_hash.indexOf('nMenuSw') != -1)) {
        $(".navBar").css("display","none");
        $("#bottomGrp").css("display","none");
    } else {
        $(".navBar").css("display","block");
        $("#bottomGrp").css("display","block");
    }


}

