/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
/* 화면 크기에 따른 모바일 / PC 판별 변수 */
var devChk = "";
var preDevChk = "";

var coverImgSizeStr="";
var imgSizeStr = "";
var thumbSizeStr = "";

var mobileCoverImgSizeStr="&width=640&height=480"
var pcThumbSizeStr = "&width=160&height=120";
var mobileImgSizeStr="&width=584&height=438";
var mobileThumbSizeStr="&width=80&height=60";

var coverImgPath;
var thumbList;
var lastModel;
/* 화면 width */
var screenWidth = window.innerWidth;
var timer1,timer2;
var endTimer;
$(window).resize(function(){
    real_mobile_check();
    lastModel = undefined;
    fotomaraChk();


    $(".conTitle").mCustomScrollbar("stop");
    $(".conTitle").mCustomScrollbar("destroy");

    clearTimeout(timer1);
    clearTimeout(timer2);
    $(".conTitle").mCustomScrollbar({ axis : "x", theme:"minimal",
        advanced:{ updateOnContentResize: true },
        callbacks:{
            onScrollStart : function(){
                clearTimeout(endTimer);
            },
            onScroll:function(){

                if (scrollStatus == "rightEnd") {
                    timer1 = setTimeout(function(){
                        $(".contTitle").mCustomScrollbar("scrollTo","0",{
                            scrollInertia:0
                        });
                    },2000);
                    scrollStatus = "rightStart";
                } else if (scrollStatus == "rightStart"){
                    timer2 = setTimeout(function(){
                        $(".contTitle").mCustomScrollbar("scrollTo","right",{
                            scrollInertia:8000
                        });
                    },2000);

                    scrollStatus = "rightEnd";
                } else if (scrollStatus == "dragStart"){
                    scrollStatus = "rightEnd";
                }
            },
            onInit:function(){
                timer2 = setTimeout(function(){
                    $(".contTitle").mCustomScrollbar("scrollTo","right",{
                        scrollInertia:8000
                    });
                    $(".mCSB_dragger .mCSB_dragger_bar").css('width', '0px', 'important');
                },2000);
                scrollStatus = "rightEnd";
            }
        }
    });

    if(preDevChk != devChk) {
        changeInitCoverImage(coverImgPath); // parameter : isChange, coverImgPath
        changeInitFotoramaImgThumbSize(true, thumbList); // parameter : isChange, thumbnailList
        preDevChk = devChk;
    }
});

$(document).ready(function(){

    fotomaraChk();
    preDevChk = devChk;

    top.document.title = "";

    contentInfo();
    real_mobile_check();

    $(window).on('hashchange', function () {
        real_mobile_check();
    });
});

contentInfo = function(){
    var str_hash = document.location.hash.replace("#", "");
    var mode = "";
    if (str_hash.indexOf('nMenuSw') != -1) {
        mode = "o_1";
    }

    callByGet("/api/contents?contsSeq="+$("#contsSeq").val()+"&mode="+mode, "contentsInfoSuccess");
}
var scrollStatus = "Init";
contentsInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.contentsInfo;
        top.document.title = item.firstCtgNm + " : GiGA Live On";

        //var coverImg = makeAPIUrl(item.coverImgPath, "img");

        $("#coverImg").html("<img src='" + makeAPIUrl(item.coverImgPath, "img") + "' width='100%'>");
        $("#contsTitle").html(item.contsTitle.toUpperCase());
        var str = item.secondCtgNm.split("_");
        $("#compat").html(str[0]);
        $("#compat_m").html(str[0]);

        var genreHtml = "";
        $(data.result.contentsInfo.genreList).each(function(i, item){
            genreHtml += item.genreNm + "/";
        });
        $("#genreList").html("   <div class='div'>></div>   " + item.firstCtgNm + "   <div class='div'>></div>   " + genreHtml.slice(0, -1));

        $("#contsDesc").html("<span class='h_Info'>"+item.contsDesc+"</span>");

        $("#cpNm").html(item.cpNm);
        $("#cpNm_m").html(item.cpNm);

        $("#contsVer").html(item.contsVer);
        $("#contsVer_m").html(item.contsVer);

//        $("#fileSize").html(toMB(item.fileSize) + "MB");
//        $("#fileSize_m").html(toMB(item.fileSize) + "MB");

        $("#maxAvlNop").html(item.maxAvlNop+"인");
        $("#maxAvlNop_m").html(item.maxAvlNop+"인");

        var str = item.firstCtgNm.split("_");
        var remainTxt = item.firstCtgNm.replace(str[0]+"_","");
        var contCtgNmTxt = str[0];
        if (remainTxt != "") {
            contCtgNmTxt += "(";
            contCtgNmTxt += remainTxt;
            contCtgNmTxt += ")";
        }

        $("#contCtgNm").html(contCtgNmTxt + " > " + genreHtml.slice(0, -1));
        $("#contCtgNm_m").html(contCtgNmTxt + " > " + genreHtml.slice(0, -1));

        var serviceHtml = "";
//        $(data.result.contentsInfo.serviceList).each(function(i, item){
//            serviceHtml += item.svcNm + "/";
//        });
//        $("#serviceList").html(serviceHtml.slice(0, -1));
        $("#cretDt").html(item.amdDt);
        $("#cretDt_m").html(item.amdDt);

        $("#fileType").html(item.fileType);
        if (item.fileType == "VIDEO") {
            $(".exeFilePath").hide();
            $(".contentInfo").show();
        }
        $("#exeFilePath").html(item.exeFilePath);

        var videoHtml = "";
        var metadataHtml = "";
        var contsHtml = "";

        $(data.result.contentsInfo.videoList).each(function(i, item){
            videoHtml += "<p><span class='float_l'>" + (i+1) + ". " + item.fileNm + "(" + item.fileSize + "Byte)</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.fileSeq + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            metadataHtml += "<p><span class='float_l'>" + item.metadataInfo.fileNm + "</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.metadataInfo.fileSeq + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            contsHtml += "<p><span class='float_l'>" + item.contsXmlInfo.fileNm + "</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.contsXmlInfo.fileSeq + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
        });

        $(data.result.contentsInfo.prevList).each(function(i,item){
            $("#prevList").html("<p><span class='float_l'>" + (i+1) + ". " + item.fileNm + "(" + item.fileSize + "Byte)</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.fileSeq + "' class='fontblack btnDownloadGray'>다운로드</a></p>");
        });

        thumbList = data.result.contentsInfo.thumbnailList;
        changeInitFotoramaImgThumbSize(false, thumbList);
        /* - 썸네일 이미지 수정 전 원본코드
        var $fotoramaDiv = $('#fotorama').fotorama();
        var fotorama = $fotoramaDiv.data('fotorama');
        $(data.result.contentsInfo.thumbnailList).each(function(i, item){
            fotorama.push({img: makeAPIUrl(item.thumbnailPath, "img"), thumb: makeAPIUrl(item.thumbnailPath, "img")});
        });
        */

        var fileHtml = "";

        if (videoHtml == "") {
            $("#videoList").parents("tr").hide();
            $("#metadataName").parents("tr").hide();
            $(data.result.contentsInfo.fileList).each(function(i, item){
                fileHtml = "<p><span class='float_l'>" + item.fileNm + "&nbsp;</span><a href=\""+makeAPIUrl("/api/fileDownload/")+item.fileSeq + "\" class=\"fontblack btnDownloadGray\">[다운로드]</a></p>";
                $("#file").html(fileHtml);
            });
        } else {
            $("#videoList").html(videoHtml);
            $("#metadataName").html(metadataHtml);
            $("#contsInfo").html(contsHtml);
            $(".contsInfo").show();
            $("#file").parents("tr").hide();
        }
        listBack($(".btnList"));
        coverImgPath = item.coverImgPath;
        changeInitCoverImage(coverImgPath);
        /*  - 썸네일 이미지 수정 전 원본코드
        document.getElementById("subImage").style.backgroundImage = "url('" + coverImg + "')";
        document.getElementById("subImage").style.backgroundSize = "cover";
        */

        $(".conTitle").mCustomScrollbar({ axis : "x", theme:"minimal",
            advanced:{
                updateOnContentResize: true,
                releaseDraggableSelectors: ".mCSB_dragger"
            },
            callbacks:{
                onScrollStart : function(){
                    clearTimeout(endTimer);
                },
                onScroll:function(){
                    if (scrollStatus == "rightEnd") {
                        timer1 = setTimeout(function(){
                            $(".contTitle").mCustomScrollbar("scrollTo","0",{
                                scrollInertia:0
                            });
                        },2000);
                        scrollStatus = "rightStart";
                    } else if (scrollStatus == "rightStart"){
                        timer2 = setTimeout(function(){
                            $(".contTitle").mCustomScrollbar("scrollTo","right",{
                                scrollInertia:8000
                            });
                        },2000);

                        scrollStatus = "rightEnd";
                    } else if (scrollStatus == "dragStart"){
                        scrollStatus = "rightEnd";
                    }
                },
                onInit:function(){
                    timer2 = setTimeout(function(){
                        $(".contTitle").mCustomScrollbar("scrollTo","right",{
                            scrollInertia:8000
                        });
                        $(".mCSB_dragger .mCSB_dragger_bar").css('width', '0px', 'important');
                    },2000);
                    scrollStatus = "rightEnd";
                }
            }
        });

        $("#cpNm").mCustomScrollbar({ axis : "x", theme:"minimal" });
        $("#cpNm_m").mCustomScrollbar({ axis : "x", theme:"minimal" });

        $("#contCtgNm").mCustomScrollbar({ axis : "x", theme:"minimal" });
        $("#contCtgNm_m").mCustomScrollbar({ axis : "x", theme:"minimal" });

        $("#contsDesc").mCustomScrollbar({ axis : "xy", theme:"minimal", mouseWheel:{enable : true}});
    } else {
       top.document.title = getMessage("menu.contents.title") + " : GiGA Live On";
   }
}

backContentsDisplayList = function(){
    var keyword = "";
    var firstCtgID = window.location.pathname.replace(makeAPIUrl("/display/"), "");

    callByGet("/api/contentsDisplay?firstCtgID=" + firstCtgID + "&searchString=" + keyword, "contentsDisplayListSuccess", "NoneForm");
}

/* 화면 크기에 따른 썸네일 이미지 크기 변경*/
fotomaraChk = function(){
    /*갤러리 리스트 이미지 사이즈*/
    var fotorama_W = 80;
    var fotorama_H = 60;

    /*갤러리 이미지 사이즈*/
    var fotorama_GW = window.innerWidth-20;
    var fotorama_GH = fotorama_GW  * 0.75;

    /* 썸네일 이미지  M / PC 구분  규격 변경*/
    screenWidth = window.innerWidth;

    /* 화면 크기에 따른 기기 판별 후 변수 저장*/
    if (screenWidth <= 640) {
        /* 중복 변경 방지*/
        if (devChk == "mobile") {
//            return;
        }

        devChk = "mobile";

    } else {
        /* 중복 변경 방지*/
        if (devChk == "pc") {
//            return;
        }

        devChk = "pc";
    }

    if (devChk == "pc") {
        fotorama_W = 160;
        fotorama_H = 120;
        if(window.innerWidth >= 1058){
            fotorama_GW = 1040;
            fotorama_GH = 780;
        }
    }

    $('.fotorama').fotorama({
        thumbwidth: fotorama_W,
        thumbheight: fotorama_H,
        width:fotorama_GW,
        height:fotorama_GH,
        ratio:"4/3",
        maxwidth:"100%",
        nav:"thumbs"
    });
}

changeInitCoverImage = function(coverImgPath) {
    if (devChk == "pc") {
        coverImgSizeStr = "";
    } else {
        coverImgSizeStr = mobileCoverImgSizeStr;
    }
    var coverImgUrl = makeAPIUrl(coverImgPath + coverImgSizeStr, "img");

    document.getElementById("subImage").style.backgroundImage = "url('" + coverImgUrl + "')";
    document.getElementById("subImage").style.backgroundSize = "cover";
}

changeInitFotoramaImgThumbSize = function(isChange, thumbList) {
    if (devChk == "pc") {
        imgSizeStr = "";
        thumbSizeStr = pcThumbSizeStr;
    } else {
        imgSizeStr = mobileImgSizeStr;
        thumbSizeStr = mobileThumbSizeStr;
    }

    var $fotoramaDiv = $('#fotorama').fotorama();
    var fotorama = $fotoramaDiv.data('fotorama');

    if (isChange) {
        fotorama.splice(0, thumbList.length);
    }

    $(thumbList).each(function(i, item){
        fotorama.push({img: makeAPIUrl(item.thumbnailPath + imgSizeStr, "img"), thumb: makeAPIUrl(item.thumbnailPath + thumbSizeStr, "img")});
    });
}


function real_mobile_check(){
    var chk = "default";
    var isMobile = {
        Android: function () {return navigator.userAgent.match(/Android/i) == null ? false : true;},
        BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i) == null ? false : true;},
        IOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i) == null ? false : true;},
        Opera: function () {return navigator.userAgent.match(/Opera Mini/i) == null ? false : true;},
        Windows: function () {return navigator.userAgent.match(/IEMobile/i) == null ? false : true;},
        any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.IOS() || isMobile.Opera() || isMobile.Windows());}
    };
    if(isMobile.any()){
        chk = "mobile";
        if(isMobile.Android()){
        }else if(isMobile.IOS()){
        }else if(isMobile.BlackBerry()){
        }else if(isMobile.Opera()){
        }else if(isMobile.Windows()){
        }
    }

    var str_hash = document.location.hash.replace("#","");
    var onlink = $(".btnList").attr("onclick");

    if (chk == "mobile" && (str_hash.indexOf('nMenuVw') != -1 || str_hash.indexOf('nMenuSw') != -1)) {
        $(".navBar").css("display","none");
        $("#bottomGrp").css("display","none");
    } else {
        $(".navBar").css("display","block");
        $("#bottomGrp").css("display","block");
        if (typeof onlink != "undefined") {
            $(".btnList").attr("onclick",$(".btnList").attr("onclick").replace("#nMenuVw",""));
            $(".btnList").attr("onclick",$(".btnList").attr("onclick").replace("#nMenuSw",""));
        }
    }

    if (typeof onlink != "undefined") {
        if (chk == "mobile" && str_hash.indexOf('nMenuSw') != -1 && onlink.indexOf('nMenuSw') == -1) {
            $(".btnList").attr("onclick",$(".btnList").attr("onclick").replace("#nMenuVw",""));
            $(".btnList").attr("onclick",$(".btnList").attr("onclick").replace("')","#nMenuSw')"));
        } else if (chk == "mobile" && str_hash.indexOf('nMenuVw') != -1 && onlink.indexOf('nMenuVw') == -1) {
            $(".btnList").attr("onclick",$(".btnList").attr("onclick").replace("#nMenuSw",""));
            $(".btnList").attr("onclick",$(".btnList").attr("onclick").replace("')","#nMenuVw')"));
        }
    }

}
