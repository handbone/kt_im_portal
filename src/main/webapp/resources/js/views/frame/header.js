/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var connIp = "";
var sessnId = "";
var connDvcType = "";

$(document).ready(function(){
    accessLogInfo();
    contsCtgList();

    var urlPath = location.pathname;

    if (urlPath.match("intro")) {
        $("#sideMenu").children("li:not(.sidebarSubMenuBg):eq(0)").find("a").addClass("menuSelected");
    } else if (urlPath.match("display") || urlPath.match("contents")) {
        $("#sideMenu").children("li:not(.sidebarSubMenuBg):eq(1)").find("a").addClass("menuSelected");
        $("#sideMenu").find(".sidebarSubMenuBg:eq(0)").addClass("menuSelected");
    } else if (urlPath.match("notice")) {
        $("#sideMenu").children("li:not(.sidebarSubMenuBg):eq(2)").find("a").addClass("menuSelected");
        $("#sideMenu").find(".sidebarSubMenuBg:eq(1)").addClass("menuSelected");
        $("#sideMenu").find(".sidebarSubMenuBg:eq(1)").find(".sidebarSubmenu > ul > li:eq(0) > a").addClass("menuSelected");
    } else if(urlPath.match("partnership")){
        $("#sideMenu").children("li:not(.sidebarSubMenuBg):eq(2)").find("a").addClass("menuSelected");
        $("#sideMenu").find(".sidebarSubMenuBg:eq(1)").addClass("menuSelected");
        $("#sideMenu").find(".sidebarSubMenuBg:eq(1)").find(".sidebarSubmenu > ul > li:eq(1) > a").addClass("menuSelected");
    }


    /* 메뉴 탭키 누를시 포커스 처리*/
    var removeBarMenu = false;
    $("html").keydown(function (e) {
        if (e.which == 9) {
            if(e.shiftKey) {
                if($('.menuChdTab').find("a:focus").index() != -1) {
                    if ($('a:focus').parent("li").prev().index() == -1) {
                        removeBarMenu = true; // bar item에서 다음 이동할 메뉴가 없으면 keyup 이벤트에서 display:none으로 속성을 변경토록 flag 설정
                    } else {
                        $('a:focus').parent("ul").css("display","block");
                    }
                }/* else if ( $('.bar-item').is( ":focus" )) {
                    $('.bar-item:not(.bar-item:focus) +ul').css("display","none");
                    $('.bar-item:focus +ul').css("display","block");
                } else {
                    $('.bar-item:not(.bar-item:focus) +ul').css("display","none");
                }*/
            } else {
                if($('.menuChdTab').find("a:focus").index() != -1) {
                    if ($('a:focus').parent("li").next().index() == -1) {
                        removeBarMenu = true; // bar item에서 다음 이동할 메뉴가 없으면 keyup 이벤트에서 display:none으로 속성을 변경토록 flag 설정
                    } else {
                        $('a:focus').parent("ul").css("display","block");
                    }
                } else if ( $('.bar-item').is( ":focus" )) {
                    $('.bar-item:not(.bar-item:focus) +ul').css("display","none");
                    $('.bar-item:focus +ul').css("display","block");
                } else {
                    $('.bar-item:not(.bar-item:focus) +ul').css("display","none");
                }
            }
        }
    });

    $("html").keyup(function (e) {
        if (e.which == 9) {
            if (removeBarMenu) {
                $('.bar-item +ul').css("display","none");
                removeBarMenu = false;
            }
        }
    });
});

$(window).scroll(function(){   //스크롤 최상단시 메뉴 배경 투명 처리
    if($(window).scrollTop() == 0){
        $("#myNavbar").removeClass("topBlack");
        $("#myNavbar").addClass("topTransparent");
    } else {
        $("#myNavbar").removeClass("topTransparent");
        $("#myNavbar").addClass("topBlack");
    }
});


accessLogInfo = function(){
    callByGet("/api/conninfo", "accessLogInfoSuccess");
}

accessLogInfoSuccess = function(data) {
    var filter = "win16|win32|win64|mac";

    if(navigator.platform){
        if(0 > filter.indexOf(navigator.platform.toLowerCase())){
            connDvcType = "M";
        } else {
            connDvcType = "W";
        }
    }

    if (data.resultCode == "1000") {
        var item = data.result.connectInfo;

        connIp = item.connIp;
        sessnId = item.sessnId;

        if(connDvcType == null || connDvcType == "") {
            return;
        }

        if(connIp == null || connIp == "") {
            return;
        }

        if(sessnId == null || sessnId == "") {
            return;
        }

        formData("NoneForm", "connIp", encodeURI(connIp));
        formData("NoneForm", "sessnId", encodeURI(sessnId));
        formData("NoneForm", "connDvcType", encodeURI(connDvcType));
        callByGet("/api/accesslog", "accessLogRegisterSuccess", "NoneForm");
    }
}


accessLogRegisterSuccess = function(data) {
    formDataDelete("NoneForm", "connIp");
    formDataDelete("NoneForm", "sessnId");
    formDataDelete("NoneForm", "connDvcType");
    if (data.resultCode == "1000") {
    } else if(data.resultCode == "1010"){
    }


}
contsCtgList = function(){
    callByGet("/api/contentsCategory?GroupingYN=Y", "contsCtgListSuccess", "NoneForm");
}
contsCtgListSuccess = function(data){
    if(data.resultCode == "1000"){
        var firstCtgNmListHtml= "";
        var ary = sortResults(data.result.firstCtgList,"firstCtgNm",false);
        $(ary).each(function(i,item) {
            firstCtgNmListHtml += "<li><a href='" + makeAPIUrl("/display/"+item.firstCtgID)+"' class='bar-item button'>"+xssChk(item.firstCtgNm)+"</a></li>";
        });

        $(".contsCtgList").html(firstCtgNmListHtml);

    }
    var urlPath = location.pathname;
    $('#sideMenu').find('a[href="' + urlPath + '"]').addClass('menuSelected');
}

/* json 정렬 */
function sortResults(arr,prop, asc) {
    arr = arr.sort(function(a, b) {
        if (asc) return a[prop] > b[prop] ? -1 : a[prop] > b[prop] ? 1 : 0;
        else return b[prop] > a[prop] ? -1 : a[prop] > b[prop] ? 1 : 0;
    });
    return arr;
}
/* json 정렬 */


function moveToLink(url){
    accessLogInfo();
    window.open(url, '_blank').focus();
}

function firstContMove(){
    var hrfTxt = $("#sideMenu").find(".sidebarSubMenuBg:eq(0)").find(".contsCtgList > li:eq(0) > a").attr("href");
    if (typeof hrfTxt == "undefined") {
        alert(getMessage("common.nodata.msg"));
    } else {
        hrfTxt = hrfTxt.replace("javascript:pageMove('","").replace("')","");
        location.href = hrfTxt;
    }
}
