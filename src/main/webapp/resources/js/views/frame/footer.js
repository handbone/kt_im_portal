/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

/** Navbar S **/
// Toggle between showing and hiding the sidebar when clicking the menu icon
var mySidebar = document.getElementById("mySidebar");

function ims_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        menu.style.display='block';
    } else {
        mySidebar.style.display = 'block';
        menu.style.display = 'none';
    }
}

//Close the sidebar with the close button
function ims_close() {
    mySidebar.style.display = "none";
    menu.style.display = 'block';
}

function popAlertLayer(msg, url,title,centerBtn, targetId){
    title = typeof title !== 'undefined' ? title : getMessage("common.noice");
    centerBtn = typeof centerBtn !== 'undefined' ? centerBtn : false;

    if(url){
        swal({
            title: title,
            text: msg,
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: getMessage("common.confirm")
        })
        .then(
            function(value){
                if(value){

                    swal.close();
                    if (typeof url == "function") {
                        url();
                    } else {
                        pageMove(url);
                    }
                } else {
                    swal({
                        title: getMessage("common.noice"),
                        text: msg,
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        buttons: getMessage("common.confirm")
                    });
                }
            }
        );
    } else {
        swal({
            title: title,
            text: msg,
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: getMessage("common.confirm")
        })
        .then(
                function(value) {
                    if (targetId) {
                        document.getElementById(targetId).focus();
                    }
                }
        );
    }

    if (centerBtn) {
        $(".swal-footer").css("text-align","center");
    } else {
        $(".swal-footer").css("text-align","right");
    }

    $(".swal-text").html($(".swal-text").text());
}

// sub menu down

$('#menu li').hover(
    function() {
        $('ul', this).stop().slideDown(200);
        $(this).find("li").css("min-width",$(this).width());
    },
    function() {
        $('ul', this).stop().slideUp(0);
    }
);
