<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<script type="text/javascript" src="<c:url value='/resources/js/views/frame/footer.js?version=201805090933' />"></script>
    <form id="NoneForm"></form>
<div id="bottomGrp">
    <ul>
        <li>주식회사 케이티</li>
        <li>사업자 등록번호 : 102-81-42945</li>
        <li>대표이사 황창규</li>
        <li class="noBorder">문의 : <a href="mailto:ktimms@kt.com">ktimms@kt.com</a></li>
    </ul>
</div>

</body>
</html>