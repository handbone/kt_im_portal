<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE HTML>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />

<title></title>

<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/jquery/jquery-ui.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/jquery/jquery-ui-timepicker-addon.css?version=201805101011">
<link href="<%=request.getContextPath()%>/resources/css/jqGrid/ui.jqgrid.css?version=12313" rel="stylesheet" type="text/css">
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-1.7.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-ui-1.10.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery.form.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery.cookie.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jqGrid/grid.locale-en.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jqGrid/jquery.jqGrid.src.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/apiFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/baseFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/commonFunc.js?version=201805251960"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/cookie.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/views/frame/header.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/sweetalert/sweetalert.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/message.js?time=<%=session.getLastAccessedTime()%>"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery-parallax/jquery.parallax.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/default.css?version=20180829">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/impd.css?version=20180829">

<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/jquery-parallax/jquery.parallax.css" />

</head>

<body>
<input type="hidden" id="contextPath" value="<%=request.getContextPath()%>">
<!-- Navbar S -->
<div id="menu" class="top">
    <!-- Navbar links S -->
    <div class="navBar">
        <div class="innerBar hide-small">
            <div class="logo"><a href="javascript:;" onclick="pageMove('/')"><img src="<%=request.getContextPath()%>/resources/image/logo.png" alt="<spring:message code="menu.home.title" />"></a></div>
            <ul>
                <li><a href="javascript:;" onclick="pageMove('/intro')" class="bar-item button"><spring:message code="menu.intro.title" /></a></li>
                <li><a href="javascript:;" onclick="firstContMove()" class="bar-item button"><spring:message code="menu.contents.title" /></a>
                    <ul class="contsCtgList menuChdTab">
                        <!-- contsCtgList -->
                    </ul>
                </li>
                <li><a href="javascript:;" onclick="pageMove('/notice')" class="bar-item button"><spring:message code="menu.customer.title" /></a>
                    <ul class='menuChdTab'>
                        <li><a href="javascript:;" onclick="pageMove('/notice')"><spring:message code="menu.customer.notice" /></a></li>
                        <li><a href="javascript:;" onclick="pageMove('/partnership')"><spring:message code="menu.customer.partnership" /></a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="bar-item button"><spring:message code="menu.service.title" /></a>
                    <ul class='menuChdTab' style="width:max-content;">
                        <li><a href="javascript:;" onclick="moveToLink('http://www.vright.com/')"><spring:message code="menu.service.vright" /></a></li>
                        <li><a href="javascript:;" onclick="moveToLink('http://www.gigalivetv.co.kr/')"><spring:message code="menu.service.gigalivetv" /></a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div  id="myNavbar" class="myNavBarBG topTransparent"></div>

        <!-- Menu Icon S  -->
        <div class="logo hide-large" style="position:absolute; left:20px; z-index:1110;"><a href="javascript:;" onclick="pageMove('/')"><img src="<%=request.getContextPath()%>/resources/image/logo.png" alt="<spring:message code="menu.home.title" />"></a></div>
        <a href="javascript:void(0)" class="bar-item button right hide-large hide-medium" onclick="ims_open()" style="position:absolute; z-index:1100; right:15px;"><img src="<%=request.getContextPath()%>/resources/image/icon_menu.png" alt="MENU"></a>
    <!-- Navbar links E -->
    </div>
</div>


<!-- Sidebar on small screens when clicking the menu icon -->

<nav class="sidebar animate-right" style="display:none; right:0; padding:0px; padding-top:50px;" id="mySidebar">
    <div class="sidebarLogo"><img src="<%=request.getContextPath()%>/resources/image/logo.png" alt="<spring:message code="menu.home.title" />"></div>
    <div class="btnClose"><a href="javascript:void(0)" onclick="ims_close()" class=""><img src="<%=request.getContextPath()%>/resources/image/btn_close.png" alt="닫기"></a></div>

    <ul id="sideMenu">
        <li>
            <a href="javascript:pageMove('/intro')" onclick="ims_close()" class="bar-item button">
                <spring:message code="menu.intro.title" />
            </a>
        </li>
        <li>
            <a href="javascript:firstContMove();" onclick="ims_close()" class="bar-item button">
                <spring:message code="menu.contents.title" />
            </a>
        </li>
        <li class="sidebarSubMenuBg">
            <div class="sidebarSubmenu">
                <ul class="contsCtgList">
                    <!-- contsCtgList -->
                </ul>
            </div>
        </li>
        <li>
            <a href="javascript:pageMove('/notice')" onclick="ims_close()" class="bar-item button">
                <spring:message code="menu.customer.title" />
            </a>
        </li>
        <li class="sidebarSubMenuBg">
            <div class="sidebarSubmenu">
                <ul>
                    <li><a href="javascript:pageMove('/notice')" class="bar-item button"><spring:message code="menu.customer.notice" /></a></li>
                    <li><a href="javascript:pageMove('/partnership')" class="bar-item button"><spring:message code="menu.customer.partnership" /></a></li>
                </ul>
            </div>
        </li>
        <li>
            <a href="javascript:void(0);" onclick="ims_close()" class="bar-item button">
                <spring:message code="menu.service.title" />
            </a>
        </li>
        <li class="sidebarSubMenuBg">
            <div class="sidebarSubmenu">
                <ul>
                    <li><a href="javascript:moveToLink('http://www.vright.com/');" class="bar-item button"><spring:message code="menu.service.vright" /></a></li>
                    <li><a href="javascript:moveToLink('http://www.gigalivetv.co.kr/');" class="bar-item button"><spring:message code="menu.service.gigalivetv" /></a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>
<!-- Navbar E -->
