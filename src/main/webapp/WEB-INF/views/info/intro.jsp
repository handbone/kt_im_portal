<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/header.jsp" %>

<script>
top.document.title = getMessage("menu.intro.title") + " : GiGA Live On";
</script>

<!-- Sub image S -->
<div id="subImage">
    <div class="subImage sub11 txtLarge"><img src="<c:url value='/resources/image/sub_title1.png'/>" alt="<spring:message code="main.title.intro"/>"></div>
    <div class="subImageMobile" style="display:none;">
        <img src="<c:url value='/resources/image/sub_image_introduce.jpg'/>" width="100%" alt="<spring:message code="main.title.intro"/>">
    </div>
</div>
<!-- Sub image E -->

<!--  Contents S  -->
<div id="contents">
    <div class="conImage conImage1">
        <div class="conTxt conTxt500">
            <div class="conTxtTitle">INTRODUCE</div>
            <div class="conTxtSubject1 conTxtSubject1">GiGA Live On 소개</div>
            <div class="conTxtText">GiGA Live On은 콘텐츠를 제작하고 배포하고자 하는 개발사와 플레이를 제공하는 서비스사를 연결하여 고객들이 편리하게 VR/AR 콘텐츠를 체험할 수 있도록 지원합니다.</div>
        </div>
    </div>
    <div class="conImage conImage2">
        <div class="conTxt conTxt500  alignRight ">
            <div class="conTxtTitle">MANAGEMENT</div>
            <div class="conTxtSubject2 conTxtSubject2">제작사 및 서비스사 활용성</div>
            <div class="conTxtText">콘텐츠 제작사는 GiGA Live On을 통해 이용현황, 메타정보 등 손쉬운 통합 관리를 할 수 있으며,
             서비스사는 매장 내 콘텐츠 관리는 물론, 원하는 콘텐츠를 결합하여 다운로드, 플레이를 제공할 수 있습니다.</div>
        </div>
    </div>
    <div class="conImage conImage3">
        <div class="conTxt conTxt500 alignLeft" style="width:100%; padding:0;">
            <div class="conTxtTitle">HOW IT WORKS</div>
            <div class="conTxtSubject3">GiGA Live On 서비스 플로우</div>
            <!-- <div class="conTxtText"> 검수 및 승인 단계를 거쳐 스토어에 등록하면 고객들에게 콘텐츠를 제공할 수 있습니다. 금주의 핫 콘텐츠, 이 달의 추천 콘텐츠 등 원하는대로 콘텐츠를 편성하여  고객들에게 제공할 수 있습니다.</div>-->
        </div>
        <div class="conTxtImg"><img src="<c:url value='/resources/image/introduce_img3.png'/>" alt="CP사 (콘텐츠유통) 검수 및 승인 단계를 거쳐 스토어에 등록하여 고객에게 다양한 콘텐츠 전달, 서비스사 (콘텐츠 편성 및 매장 운영) 금주의  HOT 콘텐츠, 이 달의 추천 등 원하는 대로 콘텐츠 편성, 사용자 (VR/AR/MR 플레이) 매장을 방문한 사용자들에게 편리한 PLAY 환경 제공" width="100%" height="100%"></div>
        <div class="conTxtImgMoble" style="display:none;"><img src="<c:url value='/resources/image/introduce_img3_m.png'/>" alt="CP사 (콘텐츠유통) 검수 및 승인 단계를 거쳐 스토어에 등록하여 고객에게 다양한 콘텐츠 전달, 서비스사 (콘텐츠 편성 및 매장 운영) 금주의  HOT 콘텐츠, 이 달의 추천 등 원하는 대로 콘텐츠 편성, 사용자 (VR/AR/MR 플레이) 매장을 방문한 사용자들에게 편리한 PLAY 환경 제공" width="100%" height="100%"></div><br><br>
    </div>



</div>
<!--  Contents E  -->




<%@include file="/WEB-INF/views/frame/footer.jsp" %>

