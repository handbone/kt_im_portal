<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/header.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/home.js"></script>

<!-- ■■■■■■■■■■■■■■■■■■■■■■■   Banner S   ■■■■■■■■■■■■■■■■■■■■■■■ -->

  <style type="text/css" media="screen, projection">
    .parallax-viewport {
        width: 100%;
        max-width: 120em;
        height: 35.417em;
        background-color: #aebcc9;
    }
    #container .recommand {
        overflow: visible;
    }

    #container .conBox {
        padding-right: 28.26px;
    }

    @media screen and (max-width:640px){ /* 640px 이하  */
        #container .conBox {
            padding-right: 6.5px;
        }
    }
    </style>

<!-- Header Image S Wide -->

<div class="slideshowContainer">
    <div class="parallax-viewport " id="parallax" >

        <!-- parallax layers -->
        <div class="parallax-layer mySlides" style="width:1920px; height:720px; display: block;">
            <img src="<c:url value='/resources/image/mainbanner101.jpg'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; top:0px; left:0px;" class="main_abs_1"/>
        </div>
        <div class="parallax-layer mySlides2" style="width:1800px; height:730px; display: block;">
            <img src="<c:url value='/resources/image/mainbanner102.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_2"/>
        </div>
        <div class="parallax-layer mySlides3" style="width:1800px; height:780px; display: block;">
            <img src="<c:url value='/resources/image/mainbanner103.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_3"/>
        </div>
        <div class="parallax-layer mySlides4" style="width:1800px; height:800px; display: block;">
            <img src="<c:url value='/resources/image/mainbanner104.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_4"/>
        </div>

        
        <div class="parallax-layer mySlides" style="width:1920px; height:720px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner201.jpg'/>" alt="실감미디어 몰입형 농구 트레이닝, MR Basketball. New Extreme Sprots Park, K-live X에서 다양한 스포츠를 즐기세요!" style="position:absolute; top:0px; left:0px; height: 800px;" class="main_abs_1"/>
        </div>
        <div class="parallax-layer mySlides2" style="width:1800px; height:730px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner202.png'/>" alt="실감미디어 몰입형 농구 트레이닝, MR Basketball. New Extreme Sprots Park, K-live X에서 다양한 스포츠를 즐기세요!" style="position:absolute; left:0px; top:-20px;" class="main_abs_2"/>
        </div>
        <div class="parallax-layer mySlides3" style="width:1800px; height:780px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner203.png'/>" alt="" style="position:absolute; left:0px; top:-20px;" class="main_abs_3"/>
        </div>
        <div class="parallax-layer mySlides4" style="width:1800px; height:800px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner204.png'/>" alt="" style="position:absolute; left:0px; top:-20px;" class="main_abs_4"/>
        </div>


        <div class="parallax-layer mySlides" style="width:1920px; height:720px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner301.png'/>" alt="WIDE MAX 화면으로 더 크고 선명하게. Live On 360에서 더 많은 콘텐츠를 경험해보세요!" style="position:absolute; top:0px; left:0px;" class="main_abs_1"/>
        </div>
        <div class="parallax-layer mySlides2" style="width:1800px; height:730px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner302.png'/>" alt="WIDE MAX 화면으로 더 크고 선명하게. Live On 360에서 더 많은 콘텐츠를 경험해보세요!" style="position:absolute; left:0px; top:-20px;" class="main_abs_2"/>
        </div>
        <div class="parallax-layer mySlides3" style="width:1800px; height:780px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner303.png'/>" alt="" style="position:absolute; left:0px; top:-20px;" class="main_abs_3"/>
        </div>
        <div class="parallax-layer mySlides4" style="width:1800px; height:800px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner304.png'/>" alt="WIDE MAX 화면으로 더 크고 선명하게. Live On 360에서 더 많은 콘텐츠를 경험해보세요!" style="position:absolute; left:0px; top:-20px;" class="main_abs_4"/>
        </div>

        <!-- 
        <div class="parallax-layer mySlides" style="width:1920px; height:720px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner101.jpg'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; top:0px; left:0px;"  class="main_abs_1"/>
        </div>
        <div class="parallax-layer mySlides2" style="width:1800px; height:730px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner102.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_2"/>
        </div>
        <div class="parallax-layer mySlides3" style="width:1800px; height:780px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner103.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_3"/>
        </div>
        <div class="parallax-layer mySlides4" style="width:1800px; height:800px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner104.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_4"/>
        </div>

        <div class="parallax-layer mySlides" style="width:1920px; height:720px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner101.jpg'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; top:0px; left:0px;" class="main_abs_1"/>
        </div>
        <div class="parallax-layer mySlides2" style="width:1800px; height:730px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner102.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_2"/>
        </div>
        <div class="parallax-layer mySlides3" style="width:1800px; height:780px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner103.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_3"/>
        </div>
        <div class="parallax-layer mySlides4" style="width:1800px; height:800px; display: none;">
            <img src="<c:url value='/resources/image/mainbanner104.png'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" style="position:absolute; left:0px; top:-20px;" class="main_abs_4"/>
        </div>
         -->
    </div>

    <a class="prev" onclick="plusSlides(-1, this)" onkeydown="plusSlides(-1, this)" tabindex = "0"><img src="<c:url value='/resources/image/slide_btn_left.png'/>" alt="이전 배너 버튼" width="100%"></a>
    <a class="next" onclick="plusSlides(1, this)" onkeydown="plusSlides(-1, this)" tabindex = "0"><img src="<c:url value='/resources/image/slide_btn_right.png'/>" alt="다음 배너 버튼" width="100%"></a>

    <div class="webStatBtn">
        <a id="playBtn" class="start" onclick="playSlidesSm(this)" ></a>
    </div>
    <div style="text-align:center" class="dotGrp">
        <a href="javascript:;" class="dot" tabindex="0" id="dot1" onclick="currentSlide(1)"></a>
        <a href="javascript:;" class="dot" tabindex="0" id="dot2" onclick="currentSlide(2)"></a>
        <a href="javascript:;" class="dot" tabindex="0" id="dot3" onclick="currentSlide(3)"></a>
        <!--
        <a href="javascript:;" class="dot" tabindex="0" id="dot4" onclick="currentSlide(4)"></a>
          -->
    </div>
</div>
<!-- Header Image E -->

<!-- Header Image mobile  S -->
<div class="slideshowContainerMobile" style="none">
    <div class="myslidesSm fade bgimg1" style="display:block;">
      <img src="<c:url value='/resources/image/main_sample_m.jpg'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" width="100%">
    </div>
    
    <div class="myslidesSm fade bgimg2" style="display:none;">
     <img src="<c:url value='/resources/image/main_sample2_m.jpg'/>" alt="실감미디어 몰입형 농구 트레이닝, MR Basketball. New Extreme Sprots Park, K-live X에서 다양한 스포츠를 즐기세요!" width="100%">
    </div>

    <div class="myslidesSm fade bgimg3" style="display:none;">
      <img src="<c:url value='/resources/image/main_sample3_m.jpg'/>" alt="WIDE MAX 화면으로 더 크고 선명하게. Live On 360에서 더 많은 콘텐츠를 경험해보세요!" width="100%">
    </div>
    <!--
    <div class="myslidesSm fade bgimg4" style="display:none;">
      <img src="<c:url value='/resources/image/main_sample_m.jpg'/>" alt="현실을 뛰어넘는 눈부신 경험 제공. 편리하게 브이알과 에이알 콘텐츠를 체험할 수 있도록 지원" width="100%">
    </div>
    -->

    <a class="prev" onclick="plusSlidesSm(-1, this)" onkeydown="plusSlidesSm(-1, this)" id="prev" tabindex = "0"><img src="<c:url value='/resources/image/btn_left.png'/>" alt="이전 배너 버튼" width="100%"></a>
    <a class="next" onclick="plusSlidesSm(1, this)" onkeydown="plusSlidesSm(-1, this)" id="next" tabindex = "0"><img src="<c:url value='/resources/image/btn_right.png'/>" alt="다음 배너 버튼" width="100%"></a>
    <div style="text-align:center" class="dotGrp">
        <span id="playBtnSm" class="start" onclick="playSlidesSm()"></span>
        <a href="javascript:;" class="dotm active" tabindex="0" id="dotm1" onclick="currentSlideSm(1)" title="1번째 배너 선택됨"></a>        
        <a href="javascript:;" class="dotm" tabindex="0" id="dotm2" onclick="currentSlideSm(2)" title="2번째 배너"></a>
        <a href="javascript:;" class="dotm" tabindex="0" id="dotm3" onclick="currentSlideSm(3)" title="3번째 배너"></a>
        <!--
        <a href="javascript:;" class="dotm" tabindex="0" id="dotm4" onclick="currentSlideSm(4)" title="4번째 배너"></a>
         -->
    </div>
</div>
<!-- Header Image E -->
<!-- ■■■■■■■■■■■■■■■■■■■■■■■   Banner E   ■■■■■■■■■■■■■■■■■■■■■■■ -->

<!-- ■■■■■■■■■■■■■■■■■■■■■■■   Features S   ■■■■■■■■■■■■■■■■■■■■■■■ -->

<!-- container S -->
<div id="meritBox">
    <div id="mainSection">
        <div class="mainSectionTitle"><img src="<c:url value='/resources/image/main_txt_features.png'/>" alt="FEATURES"></div>
        <div class="proGrp">
            <div class="pro proPadLeft"><img src="<c:url value='/resources/image/main_pro_img1.png'/>" alt="EASY 새로운 VR콘텐츠의 쉬운 등록"></div>
            <div class="pro proPadCenter"><img src="<c:url value='/resources/image/main_pro_img2.png'/>" alt="HIGH 고품질의 다양한 콘텐츠 제공"></div>
            <div class="pro proPadRight"><img src="<c:url value='/resources/image/main_pro_img3.png'/>" alt="JOY 편리하고 쉬운 매장 PLAY"></div>
        </div>
    </div>
</div>

<div id="meritBoxWide">
    <div id="mainSection">
        <div class="mainSectionTitle"><img src="<c:url value='/resources/image/main_txt_features.png'/>" alt="FEATURES"></div>
        <div class="mainSectionTitleMobile" style="display:none;"><img src="<%=request.getContextPath()%>/resources/image/main_txt_features_m.png" alt="FEATURES"></div>
        <div class="proGrp">
            <div class="pro padBtm20"><img src="<c:url value='/resources/image/main_pro_img1.png'/>" alt="EASY 새로운 VR콘텐츠의 쉬운 등록"></div>
            <div class="pro padBtm20"><img src="<c:url value='/resources/image/main_pro_img2.png'/>" alt="HIGH 고품질의 다양한 콘텐츠 제공"></div>
            <div class="pro"><img src="<c:url value='/resources/image/main_pro_img3.png'/>" alt="JOY 편리하고 쉬운 매장 PLAY"></div>
        </div>
    </div>
</div>
<!-- container E -->
<!-- ■■■■■■■■■■■■■■■■■■■■■■■   Features E   ■■■■■■■■■■■■■■■■■■■■■■■ -->

<!-- ■■■■■■■■■■■■■■■■■■■■■■■   mainSection S   ■■■■■■■■■■■■■■■■■■■■■■■ -->
<!-- mainSection S -->
<div id="container" class="containerBg">
    <div id="mainSection" class="recommand">
    <!-- contents list S -->

            <div class="mainSectionTitle"><img src="<c:url value='/resources/image/main_txt_recommand.png'/>" alt="RECOMMAND"></div>
            <div class="mainSectionTitleMobile" style="display:none;"><img src="<c:url value='/resources/image/main_txt_recommand_m.png'/>" alt="RECOMMAND"></div>
            <ul id="selectRecommendContents">
            </ul>
    </div>

</div>

<!-- ■■■■■■■■■■■■■■■■■■■■■■■   mainSection E   ■■■■■■■■■■■■■■■■■■■■■■■ -->


<%@include file="/WEB-INF/views/frame/footer.jsp" %>
