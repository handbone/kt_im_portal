<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/header.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/DisplayList.js"></script>

<link href="<%=request.getContextPath()%>/resources/js/libs/slick/slick.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/js/libs/slick/slick-theme.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/slick/slick.min.js"></script>

  <style type="text/css">

    .slider {
        margin: auto;
        margin-left: 10px;
    }

    .slick-slide {
      margin: 0px 5px;
    }

    .slick-slide img {
    }

    .slick-prev:before,
    .slick-next:before {
      color: white;
    }

    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: 1;
    }

    .slick-active {
      opacity: 1;
      text-align:center;
    }

    .slick-current {
      opacity: 1;
    }
  </style>

<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="8">
<input type="hidden" id="display" value="display">



<!-- Sub image S -->
<div id="subImage">
    <div class="subImage sub21 txtLarge" style="border-bottom:solid 2px #18e4c7;">
        <img src="<c:url value='/resources/image/sub_title2.png'/>" alt="<spring:message code="main.title.contents"/>">
    </div>
    <div class="subImageMobile" style="display:none;">
        <img src="<c:url value='/resources/image/sub_image_content.jpg'/>" width="100%" alt="<spring:message code="main.title.contents"/>">
    </div>
</div>
<!-- Sub image E -->

<!--  Contents S  -->
<div class="contentsList conTxt">
    <div class="contentCat">
        <div class="regular contentCatGrp" id="contentsGenreList">
        </div>
    </div>
</div>

<div id="contents">
    <!-- Select Box S -->
    <div class="selectGrp">
        <div class="totalBox" id="contentCount"></div>
        <div class="selectBox">
            <span>
                <label for="orderSelbox"></label>
                <select id="orderSelbox" title="최신순/이름순 정렬">
                </select>
            </span>
        </div>
    </div>
    <!-- Select Box E -->


<!-- contents list Grp S -->
    <div id="container" class="contentList">
    <!-- contents list S -->
        <ul id="contentsList">
        </ul>
    </div>

 <!-- mainSection E -->
<!-- contents list GrpE -->

</div>
<!--  Contents E  -->
<style>
.contentsList .contentCatGrp  {width:100% !important}
.slick-slide{margin:0px !important;}
.contentsList .contentCat{max-width:1045px;}
.contentsList {overflow:hidden}
</style>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>