<!-- IM Platform version 1.0

Copyright ⓒ 2018 kt corp. All rights reserved.

This is a proprietary software of kt corp, and you may not use this file except in compliance with license agreement with kt corp. Any redistribution or use of this software, with or without modification shall be strictly prohibited without prior written approval of kt corp, and the copyright notice above does not evidence any actual or intended publication of such software. -->

<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/frame/header.jsp" %>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/ContentDetail.js"></script>

<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-1.10.2.min.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/libs/fotorama/fotorama.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/fotorama/fotorama.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.js"></script>

<input type="hidden" id="contsSeq" value="${contsSeq}">

<!-- Sub image S -->
<div id="subImage" class="detail" style="border:0;">
	<div class="mask detail"><!-- mask image --></div>
	<div class="maskMobile" style="display:none;"><!-- mobile mask image --> </div>
	<%--<div class="arrowGrp">
		<a>
			<img src="<c:url value='/resources/image/arrow_left.png'/>" class="left">
		</a>
		<a>
			<img src="<c:url value='/resources/image/arrow_right.png'/>" class="right">
		</a>
	</div>--%>
	<div class="conGrp">
		<img src="../resources/image/icon_box.png" alt="아이콘" class="iconBox">
		<div class="conTitle">
			<span id="contsTitle"></span>
		</div>
		<div class="infoGrp">
			<div class="conInfo" id="contsDesc"></div>
			<div class="conDetail">
				<ul>
					<li style="width:100%">
						<span class="conItem"><spring:message code="common.genreName"/></span>
						<span style="position: relative; width:100%;">
                            <div id="contCtgNm" style="position: absolute; top: 0px; left: 0px; overflow: visible; height: 34px; width: 100%; white-space: nowrap;"></div>
                        </span>
					</li>
					<li style="width:100%">
						<span class="conItem"><spring:message code="common.compatibility"/></span>
						<span id="compat"></span>
					</li>
					<%--<li>
						<span class="conItem"><spring:message code="common.fileSize"/></span>
						<span id="fileSize"></span>
					</li>--%>
					<li>
						<span class="conItem"><spring:message code="common.maxAvlNopName"/></span>
						<span id="maxAvlNop"></span>
					</li>
					<li>
						<span class="conItem"><spring:message code="common.createDate"/></span>
						<span id="cretDt"></span>
					</li>
					<li>
						<span class="conItem"><spring:message code="common.versionName"/></span>
						<span id="contsVer"></span>
					</li>
					<li>
						<span class="conItem"><spring:message code="common.cpName"/></span>
						<span style="position: relative; width:100%;">
                            <div id="cpNm" style="position: absolute; top: 0px; left: 0px; overflow: visible; height: 34px; width: 100%; white-space: nowrap;"></div>
                        </span>
					</li>
				</ul>
			</div>
			<div class="conDetailMobile" style="display:none;">
				<table>
					<tbody>
					<tr>
						<th><spring:message code="common.genreName"/></th>
						<td style="vertical-align: top;">
                            <span style="position: relative; width:100%; display:block">
                                <div id="contCtgNm_m" style="position: absolute; top: 0px; left: 0px; overflow: visible; height: 34px; width: 100%; white-space: nowrap;"></div>
                            </span>
						</td>
					</tr>
					<tr>
						<th><spring:message code="common.compatibility"/></th>
						<td><span id="compat_m"></span></td>
					</tr>
					<%--<tr>
						<th><spring:message code="common.fileSize"/></th>
						<td><span id="fileSize_m"></span></td>
					</tr>--%>
					<tr>
						<th><spring:message code="common.maxAvlNopName"/></th>
						<td><span id="maxAvlNop_m"></span></td>
					</tr>
					<tr>
						<th><spring:message code="common.createDate"/></th>
						<td><span id="cretDt_m"></span></td>
					</tr>
					<tr>
						<th><spring:message code="common.versionName"/></th>
						<td><span id="contsVer_m"></span></td>
					</tr>
					<tr>
						<th><spring:message code="common.cpName"/></th>
						<td style="vertical-align: top;">
                            <span style="position: relative; width:100%; display:block">
                                <div id="cpNm_m" style="position: absolute; top: 0px; left: 0px; overflow: visible; height: 34px; width: 100%; white-space: nowrap;"></div>
                            </span>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!--  Contents S  -->
	<div id="contents" class="contents marginTop">
		<div class="contentGallery conTxt">
			<div class="conTxtSubject">
				<img src="<c:url value='/resources/image/gallery.png'/>" alt="갤러리" class="titleGallery">
				<div class="btnBack btnList">
					<a href="javascript:void(0);"><spring:message code="common.button.back"/></a>
				</div>
				<div class="fotorama galleryGrp" id="fotorama"></div>
			</div>
		</div>
	</div>
	<!--  Contents E  -->

	<%@include file="/WEB-INF/views/frame/footer.jsp" %>

</div>
<!-- Sub image E -->

