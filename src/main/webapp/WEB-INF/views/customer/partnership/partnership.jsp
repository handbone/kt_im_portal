<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/header.jsp" %>

<script>
top.document.title = getMessage("menu.customer.partnership") + " : GiGA Live On";
</script>

<!-- Sub image S -->
<div id="subImage">
    <div class="subImage sub31 txtLarge"><img src="<c:url value='/resources/image/sub_title3.png'/>" alt="<spring:message code="main.title.customer"/>"></div>
    <div class="subImageMobile" style="display:none;">
        <img src="<c:url value='/resources/image/sub_image_support.jpg'/>" width="100%" alt="<spring:message code="main.title.customer"/>">
    </div>
</div>
<!-- Sub image E -->

<!--  Contents S  -->
<div id="contents" class="customer">
    <div class="conTxt">
        <div class="conTxtSubject"><img src="<%=request.getContextPath()%>/resources/image/title_partner.png"></div>
        <div id="partnershipGrp">
            <div class="post">GiGA Live On은 VR/AR 콘텐츠를 운영하는 기업과의 파트너쉽을 통하여 더 높은 시너지를 창출하고자 합니다.<br>
            제휴 관련 자세한 문의를 원하실 경우 아래의 절차에 따라 내용을 보내주시면, 검토 후 답변 드리도록 하겠습니다.</div>

            <div class="flow"><img src="<c:url value='/resources/image/inquire_flow.png'/>" alt="<spring:message code="partnership.flow.msg"/>" width="100%"></div>
            <div class="flowVertical" style="display:none;"><img src="<c:url value='/resources/image/inquire_flow_v.png'/>" alt="<spring:message code="partnership.flow.msg"/>" width="100%"></div>
            <div class="inquire">
                <table>
                    <caption style="display: none"><spring:message code="partnership.table.caption"/></caption>
                    <tr>
                        <th>E-mail</th><td><a href="mailto:ktimms@kt.com">ktimms@kt.com</a></td>
                    </tr>
                    <tr>
                        <th>필수작성내용</th><td>회사명, 대표자명, 대표 담당자 연락처, 대표 담당자 이메일</td>
                    </tr>
                    <tr>
                        <th>증빙서류</th><td>사업자등록증, 회사소개서</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!--  Contents E  -->








<%@include file="/WEB-INF/views/frame/footer.jsp" %>

