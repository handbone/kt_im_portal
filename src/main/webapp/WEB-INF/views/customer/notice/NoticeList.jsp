<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/header.jsp" %>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/notice/NoticeList.js"></script>
<!-- Left Menu E -->
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="13">


<!-- Sub image S -->
<div id="subImage">
    <div class="subImage sub31 txtLarge"><img src="<c:url value='/resources/image/sub_title3.png'/>" alt="<spring:message code="main.title.customer"/>"></div>
    <div class="subImageMobile" style="display:none;">
        <img src="<c:url value='/resources/image/sub_image_support.jpg'/>" width="100%" alt="<spring:message code="main.title.customer"/>">
    </div>
</div>
<!-- Sub image E -->

<!--  Contents S  -->
<div id="contents" class="customer">
    <div class="conTxt">
        <div class="conTxtSubject"><img src="<%=request.getContextPath()%>/resources/image/notice_title.png"></div>

            <!-- Table Area S -->
             <div id="gridArea" style="margin-top:20px;">
                <!-- jQGrid S -->
                <table id="jqgridData"></table>
                <!-- Page Number S -->
                <div class="pageNumberGrp" id="pageDiv"></div>
                <!-- Page Number E -->
                <!-- jQGrid E -->
            <!-- Table Area E -->

            <!-- Search Group S -->
            <div id="searchBox">
                <div class="searchGrp">
                    <div class="selectBox">
                        <select id="target" title="<spring:message code="common.search.cond"/>"></select>
                    </div>
                    <div class="searchInput">
                        <input type="text" class="remaining" max="20" id="keyword" title="<spring:message code="common.search.keyword"/>">
                    </div>
                    <div class="searchBtn">
                        <input type="button" class="btnSearch" onclick="keywordSearch()" value="<spring:message code="common.button.search"/>">
                    </div>
                </div>
               <!-- Search Group E -->
            </div>
            <!-- mainSection E -->
        </div>
        <!-- contents list GrpE -->
    </div>
</div>
<!--  Contents E  -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>