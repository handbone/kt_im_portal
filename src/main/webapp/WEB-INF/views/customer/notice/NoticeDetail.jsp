<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/header.jsp" %>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/notice/NoticeDetail.js"></script>
<input type="hidden" id="noticeSeq" value="${noticeSeq}">
<!-- Sub image S -->
<div id="subImage">
    <div class="subImage sub31 txtLarge"><img src="<c:url value='/resources/image/sub_title3.png'/>" alt="<spring:message code="main.title.customer"/>"></div>
    <div class="subImageMobile" style="display:none;">
        <img src="<c:url value='/resources/image/sub_image_support.jpg'/>" width="100%" alt="<spring:message code="main.title.customer"/>">
    </div>
</div>
<!-- Sub image E -->

<!--  Contents S  -->
<div id="contents" class="customer">
    <div class="conTxt">
        <div class="conTxtSubject"><div class="conTxtSubject"><img src="<%=request.getContextPath()%>/resources/image/notice_title.png"></div></div>
        <div class="noticeView">
            <div class="noticeViewTitle" id="noticeTitle"></div>
            <div class="noticeViewTitle noticeViewInfo" id="noticeViewInfo"></div>
            <div class="noticeViewDetail img" id="noticeViewDetail">
            </div>
        </div>
    </div>
<!--  Contents E  -->

    <!-- Button Group S -->
    <div id="btnNoticeGrp">
        <input type="button" class="btnList btnRight" value="<spring:message code="common.button.list"/>" onclick="pageMove('/notice')">
    </div>
    <!-- Button Group E -->
</div>



<%@include file="/WEB-INF/views/frame/footer.jsp"%>