/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.controller;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.portal.common.util.LocaleMessageSource;

/**
 * Handles requests for the application home page.
 */
@Component
@Controller
public class PortalController {

    @Resource(name = "localeMessageSource")
    LocaleMessageSource messageSource;

    /**
     * 메인 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(Locale locale, Model model) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/views/home");
        return mv;
    }

    /**
     * 플랫폼 소개 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/intro", method = RequestMethod.GET)
    public ModelAndView intro(ModelAndView mv) {
        mv.setViewName("/views/info/intro");
        return mv;
    }

    /**
     * 제휴문의 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/partnership", method = RequestMethod.GET)
    public ModelAndView partnership(ModelAndView mv) {
        mv.setViewName("/views/customer/partnership/partnership");
        return mv;
    }

    @RequestMapping(value = "/crossdomain.xml", method = RequestMethod.GET)
    public ModelAndView crossdomain(Locale locale, Model model) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/views/crossdomain");
        return mv;
    }

    @RequestMapping(value = "/resources/js/message.js", method = RequestMethod.GET)
    public void message(Locale locale, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Properties properties = messageSource.getProperties(locale);
        if (properties == null)
            return;

        String category = request.getParameter("category");
        response.resetBuffer();
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        StringBuilder builder = new StringBuilder();
        if (category != null && !category.isEmpty())
            builder.append("get" + category.substring(0, 1).toUpperCase() + category.substring(1)
                    + "Message = function(id) {\r\n");
        else
            builder.append("getMessage = function(id) {\r\n");

        @SuppressWarnings("rawtypes")
        Iterator iterator = properties.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            if ((category != null && !category.isEmpty()) && !key.contains(category))
                continue;
            String value = (String) properties.get(key);
            builder.append("    if (id == \"" + key + "\") {\r\n");
            builder.append("        return \"" + value + "\";\r\n");
            builder.append("    }\r\n");
        }
        builder.append("    return '';\r\n");
        builder.append("}\r\n");

        out.println(builder.toString());

        response.flushBuffer();
    }

    /* Error Page S */
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView error(Locale locale, Model model) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/views/error");
        return mv;
    }
    /* Error Page E */
}
