/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.accesslog.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.portal.accesslog.vo.AccessLogVO;
import kt.com.im.portal.common.dao.StateAbstractMapper;

/**
*
* 클래스에 대한 상세 설명
*
* @author A2TEC
* @since 2018.06.21
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 06. 21.   A2TEC      최초생성
*
*
* </pre>
*/

@Repository("AccessLogDAO")
@Transactional
public class AccessLogDAOImpl extends StateAbstractMapper implements AccessLogDAO {

    /**
     * AccessLog 정보 등록 및 수정
     * 
     * @param AccessLogVO
     * @return AccessLog 정보 등록 및 수정 성공 여부
     */
    @Override
    public int accessLogInsert(AccessLogVO vo) {
        int res = update("AccessLogDAO.accessLogInsert", vo);
        return res;
    }

}
