/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.accesslog.vo;

import java.io.Serializable;

/**
 *
 * AccessLog 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.06.21
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 21.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
public class AccessLogVO implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3996231483088969474L;

    /** 전시 접속 이력 번호 */
    int dispHstSeq;

    /** 접속 기기 구분 */
    String connDvcType;

    /** 접속 페이지 수 */
    int connPvCnt;

    /** 접속 IP */
    String connIp;

    /** Session ID */
    String sessnId;

    /** 접속 일시 */
    String connDt;


    public int getDispHstSeq() {
        return dispHstSeq;
    }

    public void setDispHstSeq(int dispHstSeq) {
        this.dispHstSeq = dispHstSeq;
    }

    public String getConnDvcType() {
        return connDvcType;
    }

    public void setConnDvcType(String connDvcType) {
        this.connDvcType = connDvcType;
    }

    public int getConnPvCnt() {
        return connPvCnt;
    }

    public void setConnPvCnt(int connPvCnt) {
        this.connPvCnt = connPvCnt;
    }

    public String getConnIp() {
        return connIp;
    }

    public void setConnIp(String connIp) {
        this.connIp = connIp;
    }

    public String getSessnId() {
        return sessnId;
    }

    public void setSessnId(String sessnId) {
        this.sessnId = sessnId;
    }

    public String getConnDt() {
        return connDt;
    }

    public void setConnDt(String connDt) {
        this.connDt = connDt;
    }

}
