/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.accesslog.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.portal.accesslog.service.AccessLogService;
import kt.com.im.portal.accesslog.vo.AccessLogVO;
import kt.com.im.portal.common.util.CommonFnc;
import kt.com.im.portal.common.util.ResultModel;

/**
 *
 * AccessLog 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.21
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 21.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class AccessLogApi {

    @Resource(name = "AccessLogService")
    private AccessLogService accessLogService;

    /**
     * Access Log 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/accesslog")
    public ModelAndView accessLogRegister(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/accesslog/accesslogProcess");
        AccessLogVO item = new AccessLogVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
        } else { // GET
            item.setConnDvcType(request.getParameter("connDvcType"));
            item.setConnIp(request.getParameter("connIp"));
            item.setSessnId(request.getParameter("sessnId"));

            int result = accessLogService.accessLogInsert(item);

            if (result == 1 || result == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CAT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            }
            rsModel.setResultType("accessLogInsert");
        }
        return rsModel.getModelAndView();
    }


    /**
     * 접속 정보 전달 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/conninfo")
    public ModelAndView connectInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/accesslog/accesslogProcess");

        request.setCharacterEncoding("UTF-8");
        AccessLogVO item = new AccessLogVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
        } else { // GET
            String connIp = CommonFnc.getClientIp(request) == null ? "" : CommonFnc.getClientIp(request);
            String sessnId = RequestContextHolder.getRequestAttributes().getSessionId() == null ? "" : RequestContextHolder.getRequestAttributes().getSessionId();
            item.setConnIp(connIp);
            item.setSessnId(sessnId);

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);

            rsModel.setData("item", item);
            rsModel.setResultType("connectInfo");
        }
        return rsModel.getModelAndView();
    }

}
