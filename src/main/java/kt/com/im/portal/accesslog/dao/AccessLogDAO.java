/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.accesslog.dao;

import org.springframework.stereotype.Repository;

import kt.com.im.portal.accesslog.vo.AccessLogVO;

/**
*
* 공통코드 관리에 관한 데이터 접근 인터페이스 클래스를 정의한다
*
* @author A2TEC
* @since 2018.06.22
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 06. 22.   A2TEC      최초생성
*
*
* </pre>
*/

@Repository("AccessLogDAO")
public interface AccessLogDAO {

    int accessLogInsert(AccessLogVO vo);

}