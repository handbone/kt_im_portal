/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.content.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.portal.common.dao.mysqlAbstractMapper;
import kt.com.im.portal.content.vo.ContentVO;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 07.   A2TEC      최초생성
 *
 *      </pre>
 */

@Repository("ContentDAO")
@Transactional
public class ContentDAOImpl extends mysqlAbstractMapper implements ContentDAO {

    /**
     * 콘텐츠 상세 정보
     * 
     * @param ContentVO
     * @return 콘텐츠 정보 (파일 / 장르 제외)
     */
    @Override
    public ContentVO contentInfo(ContentVO data) {
        return selectOne("ContentDAO.contentInfo", data);
    }

    /**
     * 콘텐츠 장르 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 장르 정보
     */
    @Override
    public List<ContentVO> contentsGenreList(ContentVO data) {
        return selectList("ContentDAO.contentsGenreList", data);
    }

    /**
     * 카테고리 별 콘텐츠 장르 전체 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 장르 정보
     */
    @Override
    public List<ContentVO> contentsGenreListAll(ContentVO data) {
        return selectList("ContentDAO.contentsGenreListAll", data);
    }

    /**
     * 콘텐츠 서비스 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 서비스 리스트 정보
     */
    @Override
    public List<ContentVO> contentServiceList(ContentVO data) {
        return selectList("ContentDAO.contentServiceList", data);
    }

    /**
     * 콘텐츠 첫번째 카테고리 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 첫번째 카테고리 리스트 정보
     */
    @Override
    public List<ContentVO> firstCtgList(ContentVO data) {
        return selectList("ContentDAO.firstCtgList", data);
    }

    /**
     * 콘텐츠 카테고리 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 카테고리 리스트 정보
     */
    @Override
    public List<ContentVO> contsCtgList(ContentVO data) {
        return selectList("ContentDAO.contsCtgList", data);
    }

    /**
     * 비디오 파일 정보 조회
     * 
     * @param ContentVO
     * @return 비디오 파일 정보 목록
     */
    @Override
    public List<ContentVO> contentsVideoList(ContentVO data) {
        return selectList("ContentDAO.contentsVideoList", data);
    }

    /**
     * 개별 컨텐츠에 대한 썸네일 이미지 정보 리스트 조회
     * 
     * @param ContentVO
     * @return 썸네일 이미지 정보 리스트 출력
     */
    @Override
    public List<ContentVO> contentsThumbnailList(ContentVO data) {
        return selectList("ContentDAO.contentsThumbnailList", data);
    }

    /**
     * 일반 파일 정보 조회
     * 
     * @param ContentVO
     * @return 일반 파일 정보 목록
     */
    @Override
    public List<ContentVO> fileDataInfo(ContentVO data) {
        return selectList("ContentDAO.fileDataInfo", data);

    }

    /**
     * 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
     * 
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
     */
    @Override
    public ContentVO contsMetaInfo(ContentVO data) {
        return selectOne("ContentDAO.contsMetaInfo", data);
    }

    /**
     * 콘텐츠 비디오 경로에 맞는 xml 파일 정보
     * 
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 xml 파일 정보
     */
    @Override
    public ContentVO metadataXmlInfo(ContentVO data) {
        return selectOne("ContentDAO.metadataXmlInfo", data);
    }

    /**
     * 콘텐츠 커버 이미지 파일 정보
     * 
     * @param ContentVO
     * @return 콘텐츠 커버 이미지 파일 정보
     */
    @Override
    public ContentVO contentCoverImageInfo(ContentVO data) {
        return selectOne("ContentDAO.contentCoverImageInfo", data);
    }

    /**
     * 파일 SEQ로 파일 정보 조회
     * 
     * @param ContentsVO
     * @return 파일 정보 출력
     */
    @Override
    public ContentVO fileInfo(ContentVO data) {
        return selectOne("ContentDAO.fileInfo", data);
    }

    /**
     * 콘텐츠 미리보기 파일 목록
     * 
     * @param ContentVO
     * @return 콘텐츠 미리보기 파일 리스트
     */
    @Override
    public List<ContentVO> contentsPrevList(ContentVO data) {
        return selectList("ContentDAO.contentsPrevList", data);
    }

    /**
     * 검색조건에 해당되는 전시 관리 목록
     *
     * @param ContentVO
     * @return 전시 목록
     */
    @Override
    public List<ContentVO> contentDisplayList(ContentVO data) {
        return selectList("ContentDAO.contentDisplayList", data);
    }

    /**
     * 검색조건에 해당되는 전시 목록의 총 개수
     *
     * @param ContentVO
     * @return 전시 목록 총 개수
     */
    @Override
    public int contentDisplayListTotalCount(ContentVO data) {
        int res = selectOne("ContentDAO.contentDisplayListTotalCount", data);
        return res;
    }

    /**
     * 추천 콘텐츠 목록
     *
     * @param ContentVO
     * @return 추천 콘텐츠 목록
     */
    @Override
    public List<ContentVO> recommendContentList(ContentVO data) {
        return selectList("ContentDAO.recommendContentList", data);
    }

    /**
     * 검색조건에 해당되는 카테고리 명
     *
     * @param ContentVO
     * @return 조회 결과
     */
    @Override
    public ContentVO searchFirstCtgNm(ContentVO data) {
        return selectOne("ContentDAO.searchFirstCtgNm", data);
    }

}
