/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.content.service;

import java.util.List;

import kt.com.im.portal.content.vo.ContentVO;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 06. 07.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public interface ContentService {

    ContentVO contentInfo(ContentVO item);

    List<ContentVO> contentsGenreList(ContentVO item);

    List<ContentVO> contentsGenreListAll(ContentVO item);

    List<ContentVO> contentServiceList(ContentVO item);

    List<ContentVO> firstCtgList(ContentVO item);

    List<ContentVO> contsCtgList(ContentVO item);

    List<ContentVO> contentsThumbnailList(ContentVO item);

    List<ContentVO> contentsVideoList(ContentVO item);

    List<ContentVO> fileDataInfo(ContentVO item);

    ContentVO metadataXmlInfo(ContentVO metaItem);

    ContentVO contentCoverImageInfo(ContentVO item);

    ContentVO fileInfo(ContentVO item);

    List<ContentVO> contentsPrevList(ContentVO item);

    ContentVO contsMetaInfo(ContentVO metaItem);

    List<ContentVO> contentDisplayList(ContentVO data);

    int contentDisplayListTotalCount(ContentVO data);

    List<ContentVO> recommendContentList(ContentVO data);

    ContentVO searchFirstCtgNm(ContentVO data);

}
