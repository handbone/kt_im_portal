/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.content.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.portal.content.dao.ContentDAO;
import kt.com.im.portal.content.vo.ContentVO;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 07.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("ContentService")
public class ContentServiceImpl implements ContentService {
    @Resource(name = "ContentDAO")
    private ContentDAO ContentDAO;

    /**
     * 콘텐츠 상세 정보
     *
     * @param ContentVO
     * @return 콘텐츠 정보 (파일 / 장르 제외)
     */
    @Override
    public ContentVO contentInfo(ContentVO item) {
        return ContentDAO.contentInfo(item);
    }

    /**
     * 콘텐츠 장르 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 장르 정보
    */
    @Override
    public List<ContentVO> contentsGenreList(ContentVO data) {
        return ContentDAO.contentsGenreList(data);
    }

    /**
     * 카테고리 별 콘텐츠 장르 전체 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 장르 정보
    */
    @Override
    public List<ContentVO> contentsGenreListAll(ContentVO data) {
        return ContentDAO.contentsGenreListAll(data);
    }

    /**
     * 콘텐츠 서비스 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 서비스 리스트 정보
    */
    @Override
    public List<ContentVO> contentServiceList(ContentVO data) {
        return ContentDAO.contentServiceList(data);
    }

    /**
     * 콘텐츠 첫번째 카테고리 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠 첫번째 카테고리 리스트 정보
    */
    @Override
    public List<ContentVO> firstCtgList(ContentVO data) {
        return ContentDAO.firstCtgList(data);
    }

    /**
     * 콘텐츠  카테고리 리스트
     * 
     * @param ContentVO
     * @return 콘텐츠  카테고리 리스트 정보
    */
    @Override
    public List<ContentVO> contsCtgList(ContentVO data) {
        return ContentDAO.contsCtgList(data);
    }

    /**
     * 콘텐츠 썸네일 이미지 목록
     * 
     * @param ContentVO
     * @return 콘텐츠 썸네일 이미지 리스트
    */
    @Override
    public List<ContentVO> contentsThumbnailList(ContentVO data) {
        return ContentDAO.contentsThumbnailList(data);
    }

    /**
     * 콘텐츠 비디오 파일 목록
     * 
     * @param ContentVO
     * @return 콘텐츠 비디오 파일 리스트
    */
    @Override
    public List<ContentVO> contentsVideoList(ContentVO data) {
        return ContentDAO.contentsVideoList(data);
    }

    /**
     * 콘텐츠 실행 파일 목록
     * 
     * @param ContentVO
     * @return 콘텐츠 실행 파일 리스트
    */
    @Override
    public List<ContentVO> fileDataInfo(ContentVO data) {
        return ContentDAO.fileDataInfo(data);
    }

    /**
     * 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
     * 
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
    */
    @Override
    public ContentVO contsMetaInfo(ContentVO data) {
        return ContentDAO.contsMetaInfo(data);
    }

    /**
     * 콘텐츠 비디오 경로에 맞는 xml 파일 정보
     * 
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 xml 파일 정보
    */
    @Override
    public ContentVO metadataXmlInfo(ContentVO data) {
        return ContentDAO.metadataXmlInfo(data);
    }

    /**
     * 콘텐츠 커버 이미지 파일 정보
     *
     * @param ContentVO
     * @return 콘텐츠 커버 이미지 파일 정보
     */
    @Override
    public ContentVO contentCoverImageInfo(ContentVO data) {
        return ContentDAO.contentCoverImageInfo(data);
    }

    /**
     * 파일 SEQ로 파일 정보 조회
     *
     * @param ContentsVO
     * @return 파일 정보 출력
     */
    @Override
    public ContentVO fileInfo(ContentVO data) {
        return ContentDAO.fileInfo(data);
    }
    /**
     * 콘텐츠 미리보기 파일 목록
     * 
     * @param ContentVO
     * @return 콘텐츠 미리보기 파일 리스트
    */
    @Override
    public List<ContentVO> contentsPrevList(ContentVO data) {
        return ContentDAO.contentsPrevList(data);
    }

    /**
     * 검색조건에 해당되는 전시 관리 목록
     *
     * @param ContentVO
     * @return 전시 목록
     */
    @Override
    public List<ContentVO> contentDisplayList(ContentVO data) {
        return ContentDAO.contentDisplayList(data);
    }

    /**
     * 검색조건에 해당되는 전시 목록의 총 개수
     *
     * @param ContentVO
     * @return 전시 목록 총 개수
     */
    @Override
    public int contentDisplayListTotalCount(ContentVO data) {
        int res = ContentDAO.contentDisplayListTotalCount(data);
        return res;
    }
    /**
     * 추천 콘텐츠 목록
     *
     * @param ContentVO
     * @return 추천 콘텐츠 목록
     */
    @Override
    public List<ContentVO> recommendContentList(ContentVO data) {
        return ContentDAO.recommendContentList(data);
    }

    /**
     * 검색조건에 해당되는 카테고리 명
     *
     * @param ContentVO
     * @return 조회 결과
     */
    @Override
    public ContentVO searchFirstCtgNm(ContentVO data) {
        return ContentDAO.searchFirstCtgNm(data);
    }

}
