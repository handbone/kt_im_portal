/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.content.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.portal.common.util.CommonFnc;
import kt.com.im.portal.common.util.ResultModel;
import kt.com.im.portal.content.service.ContentService;
import kt.com.im.portal.content.vo.ContentVO;

/**
 *
 * 콘텐츠에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.08
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 08.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class ContentApi {
    @Resource(name = "ContentService")
    private ContentService contentService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    /**
     * 콘텐츠 장르 리스트 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/genreList")
    public ModelAndView contentsGenreList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
        } else { // GET
            String firstCtgID = CommonFnc.emptyCheckString("firstCtgID", request);
            item.setFirstCtgID(firstCtgID);

            List<ContentVO> resultItem = contentService.contentsGenreListAll(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);

                rsModel.setResultType("contentsGenreList");
            }
        }
        return rsModel.getModelAndView();
    }


    /**
     * 콘텐츠 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contents")
    public ModelAndView contentsProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);

        rsModel.setViewName("../resources/api/contents/contentsProcess");
        ContentVO item = new ContentVO();
        if (request.getMethod().equals("POST")) {
        } else { // get
            int contsSeq = request.getParameter("contsSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("contsSeq"));

            /** 리스트 검색 콘텐츠 상태 값 번호 */
            String sttusSeq = CommonFnc.emptyCheckString("sttus", request);

            /** 리스트 CP사 번호 */
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);

            String mode = CommonFnc.emptyCheckString("mode", request);

            /** 제외 코드값 */
            String remoteCodes = CommonFnc.emptyCheckString("remoteCode", request);

            String[] remoteCode = remoteCodes.split(",");

            List<String> list = new ArrayList<String>();
            for (int i = 0; i < remoteCode.length; i++) {
                list.add(remoteCode[i]);
            }
            item.setList(list);

            item.setCpSeq(cpSeq);
            item.setSvcSeq(svcSeq);
            item.setSttusVal(sttusSeq);
            item.setMode(mode);
            if (contsSeq != 0) {
                item.setContsSeq(contsSeq);
                /** 해당 콘텐츠 장르 리스트 */
                List<ContentVO> genreList = contentService.contentsGenreList(item);

                /** 해당 콘텐츠 상세정보 */
                ContentVO resultItem = contentService.contentInfo(item);
                resultItem.setFileType("");
                resultItem.setExeFilePath("");

                /** 해당 콘텐츠 서비스 리스트 */
                List<ContentVO> serviceList = contentService.contentServiceList(item);

                /** 해당 콘텐츠 썸네일 이미지 리스트 */
                List<ContentVO> thumbnailList = contentService.contentsThumbnailList(item);

                /** 해당 콘텐츠 비디오 파일 리스트 */
                List<ContentVO> videoList = contentService.contentsVideoList(item);

                /** 해당 콘텐츠 실행 파일 리스트 */
                List<ContentVO> fileList = contentService.fileDataInfo(item);

                /** 미리보기 영상 파일 리스트 */
                List<ContentVO> prevList = contentService.contentsPrevList(item);

                for (ContentVO videoItem : videoList) {
                    ContentVO metaItem = new ContentVO();
                    metaItem.setContsSeq(contsSeq);
                    metaItem.setFilePath(videoItem.getFilePath());
                    ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                    ContentVO contsMetaInfo = contentService.contsMetaInfo(metaItem);
                    videoItem.setMetadataXmlInfo(metadataInfo);
                    videoItem.setContsXmlInfo(contsMetaInfo);
                }

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);

                    rsModel.setData("genreList", genreList);
                    rsModel.setData("serviceList", serviceList);
                    rsModel.setData("videoList", videoList);
                    rsModel.setData("thumbnailList", thumbnailList);
                    rsModel.setData("fileList", fileList);
                    rsModel.setData("prevList", prevList);
                }
                rsModel.setResultType("contentsInfo");
            }
        }
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 파일 포함되있는 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsdetail")
    public ModelAndView contentsDetailProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);

        rsModel.setViewName("../resources/api/contents/contentsProcess");
        ContentVO item = new ContentVO();
        if (request.getMethod().equals("POST")) {
        } else { // get
            int contsSeq = request.getParameter("contsSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("contsSeq"));

            /** 리스트 검색 콘텐츠 상태 값 번호 */
            String sttusSeq = CommonFnc.emptyCheckString("sttus", request);

            /** 리스트 CP사 번호 */
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);

            /** 제외 코드값 */
            String remoteCodes = CommonFnc.emptyCheckString("remoteCode", request);

            String[] remoteCode = remoteCodes.split(",");

            List<String> list = new ArrayList<String>();
            for (int i = 0; i < remoteCode.length; i++) {
                list.add(remoteCode[i]);
            }
            item.setList(list);

            item.setCpSeq(cpSeq);
            item.setSvcSeq(svcSeq);
            item.setSttusVal(sttusSeq);

            if (contsSeq != 0) {
                item.setContsSeq(contsSeq);
                /** 해당 콘텐츠 장르 리스트 */
                List<ContentVO> genreList = contentService.contentsGenreList(item);

                /** 해당 콘텐츠 상세정보 */
                ContentVO resultItem = contentService.contentInfo(item);

                /** 해당 콘텐츠 서비스 리스트 */
                List<ContentVO> serviceList = contentService.contentServiceList(item);

                /** 해당 콘텐츠 썸네일 이미지 리스트 */
                List<ContentVO> thumbnailList = contentService.contentsThumbnailList(item);

                /** 해당 콘텐츠 비디오 파일 리스트 */
                List<ContentVO> videoList = contentService.contentsVideoList(item);

                /** 해당 콘텐츠 실행 파일 리스트 */
                List<ContentVO> fileList = contentService.fileDataInfo(item);

                /** 미리보기 영상 파일 리스트 */
                List<ContentVO> prevList = contentService.contentsPrevList(item);

                for (ContentVO videoItem : videoList) {
                    ContentVO metaItem = new ContentVO();
                    metaItem.setContsSeq(contsSeq);
                    metaItem.setFilePath(videoItem.getFilePath());
                    ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                    ContentVO contsMetaInfo = contentService.contsMetaInfo(metaItem);
                    videoItem.setMetadataXmlInfo(metadataInfo);
                    videoItem.setContsXmlInfo(contsMetaInfo);
                }

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);

                    rsModel.setData("genreList", genreList);
                    rsModel.setData("serviceList", serviceList);
                    rsModel.setData("videoList", videoList);
                    rsModel.setData("thumbnailList", thumbnailList);
                    rsModel.setData("fileList", fileList);
                    rsModel.setData("prevList", prevList);
                }
                rsModel.setResultType("contentsInfo");
            }
        }
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 전시 관리 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsDisplay")
    public ModelAndView contentsDisplayList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
        } else { // GET
            String firstCtgID = request.getParameter("firstCtgID") == null ? "G" : request.getParameter("firstCtgID");
            item.setFirstCtgID(firstCtgID);

            String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString");

            String mode = CommonFnc.emptyCheckString("mode", request);

            item.setkeyword(searchString);
            item.setMode(mode);

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int limit = request.getParameter("rows") == null ? 8 : Integer.parseInt(request.getParameter("rows"));
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

            int page = (currentPage - 1) * limit;
            int pageEnd = page + limit - 1;

            // 정렬 항목 지정
            if (sidx.equals("CONTS_TITLE")) {
                sidx = "CONTS_TITLE";
            } else if (sidx.equals("AMD_DT")) {
                sidx = "AMD_DT";
            }
            item.setSidx(sidx);
            item.setlimit(limit);

            if (currentPage == 1) {
                item.setoffset(0);
            } else {
                item.setoffset(page);
            }

            String sttus = "06"; // 전시완료 상태인 콘텐츠만 노출
            item.setSttusVal(sttus);

            List<ContentVO> resultItem = contentService.contentDisplayList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            } else {
                int totalCount = contentService.contentDisplayListTotalCount(item);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
            }
            rsModel.setResultType("contentDisplayList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 추천 콘텐츠 목록 조회 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/recommendContents")
    public ModelAndView recommendContentList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
        } else { // GET
            List<ContentVO> resultItem = contentService.recommendContentList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setResultType("recommendContentList");
            }
        }
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 카테고리 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsCategory")
    public ModelAndView contentsCategoryProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");
        String method = CommonFnc.getMethod(request);

        if (method.equals("GET")) {
            return this.getContsCtgList(request, rsModel);
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView getContsCtgList(HttpServletRequest request, ResultModel rsModel) throws Exception {

        // 콘텐츠 등록, 수정 시에 콘텐츠 카테고리를 그룹화 할 경우
        /*
         * 첫번째 카테고리 리스트를 가져오지 않으면 해당 하는 첫번째 카테고리명을 정적 문자열로 호출해야만 하기 때문에
         * 동적으로 처리하기 위해 첫 번째 리스트를 가져온 다음 해당하는 두 번째 리스트를 가져오게 구성되어 있음
         */

        ContentVO item = new ContentVO();

        /** 카테고리 그룹핑 검색 여부 */
        String GroupingYn = request.getParameter("GroupingYN") == null ? "" : request.getParameter("GroupingYN");

        String unique = CommonFnc.emptyCheckString("unique", request);
        item.setGroupingYN(GroupingYn);
        item.setUnique(unique);
        if (GroupingYn.equals("Y") || GroupingYn == "Y") {
            /** 첫번째 카테고리 명 */
            String firstCtgId = request.getParameter("firstCtgID") == null ? "" : request.getParameter("firstCtgID");

            /** 콘텐츠 카테고리 번호 */
            int contsCtgSeq = request.getParameter("contsCtgSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("contsCtgSeq"));

            List<ContentVO> firstCtgList = contentService.firstCtgList(item);
            rsModel.setData("firstCtgList", firstCtgList);

            if (contsCtgSeq != 0) {
                item.setGroupingYN("N");
                item.setContsCtgSeq(contsCtgSeq);
            } else {
                if (firstCtgId == "") {
                    firstCtgId = firstCtgList.get(0).getFirstCtgID();
                }
                item.setFirstCtgID(firstCtgId);
            }
        }
        List<ContentVO> resultItem = contentService.contsCtgList(item);

        if (resultItem.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setData("item", resultItem);
        }

        if (unique.equals("on")) {
            rsModel.setResultType("contsCtgListNm");
        } else {
            rsModel.setResultType("contsCtgList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 카테고리 정보 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/categoryName")
    public ModelAndView getCategoryName(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
        } else { // GET
            String firstCtgID = request.getParameter("firstCtgID") == null ? "G" : request.getParameter("firstCtgID");
            item.setFirstCtgID(firstCtgID);

            ContentVO resultItem = contentService.searchFirstCtgNm(item);

            if (resultItem == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
            }
            rsModel.setResultType("ctgInfo");
        }
        return rsModel.getModelAndView();
    }
}
