/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.content.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 콘텐츠 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 06. 07.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
@Component
@Controller
public class ContentController {
    /**
     * 콘텐츠 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView contentsView(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/contents/ContentDetail");
        return mv;
    }

    /**
     * 전시페이지 관리 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/display/{firstCtgID}", method = RequestMethod.GET)
    public ModelAndView getDisplayList(ModelAndView mv, @PathVariable(value = "firstCtgID") String firstCtgID) {
        mv.addObject("firstCtgID", firstCtgID);
        mv.setViewName("/views/contents/DisplayList");
        return mv;
    }

}
