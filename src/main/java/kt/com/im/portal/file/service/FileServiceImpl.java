/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.file.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.portal.file.dao.FileDAO;
import kt.com.im.portal.file.vo.FileVO;

/**
 *
 * 파일 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 25.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("FileService")
public class FileServiceImpl implements FileService {

    @Resource(name = "FileDAO")
    private FileDAO FileDAO;

    /**
     * 파일 리스트 조회
     *
     * @param FileVO
     * @return 검색 조건에 부합하는 파일 리스트
     */
    @Override
    public List<FileVO> fileList(FileVO vo) {
        return FileDAO.fileList(vo);
    }

    @Override
    public FileVO fileInfo(FileVO item) {
        return FileDAO.fileInfo(item);
    }

    @Override
    public FileVO medatadataInfo(FileVO data) {
        return FileDAO.medatadataInfo(data);
    }

    @Override
    public FileVO MedatadataInfo(FileVO data) {
        return FileDAO.MedatadataInfo(data);
    }

}
