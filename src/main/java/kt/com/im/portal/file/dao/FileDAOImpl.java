/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.file.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.portal.common.dao.mysqlAbstractMapper;
import kt.com.im.portal.file.vo.FileVO;

/**
 *
 * 파일 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 25.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("FileDAO")
public class FileDAOImpl extends mysqlAbstractMapper implements FileDAO {

    /**
     * 파일 리스트 조회
     * 
     * @param FileVO
     * @return 검색 조건에 부합하는 파일 리스트
     */
    @Override
    public List<FileVO> fileList(FileVO vo) {
        return selectList("FileDAO.fileList", vo);
    }

    /**
     * contentsSeq, 파일 Type으로 파일 정보 찾기
     * 
     * @param FileVO
     * @return 파일 정보
     */
    @Override
    public FileVO fileInfo(FileVO data) {
        return selectOne("FileDAO.fileInfo", data);
    }

    /**
     * fileSeq로 파일 찾기
     * 
     * @param FileVO
     * @return 파일 정보
     */
    public FileVO medatadataInfo(FileVO data) {
        FileVO item = (FileVO) selectOne("FileDAO.medatadataInfo", data);
        return item;
    }

    /**
     * 파일 다운로드 (정보 조회)
     * 
     * @param FileVO
     * @return 파일 정보 조회
     */
    @Override
    public FileVO MedatadataInfo(FileVO data) {
        FileVO item = (FileVO) selectOne("FileDAO.medatadataInfo", data);
        return item;
    }

}
