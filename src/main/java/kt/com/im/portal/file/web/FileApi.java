/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.file.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.portal.common.util.CommonFnc;
import kt.com.im.portal.common.util.ResultModel;
import kt.com.im.portal.file.service.FileService;
import kt.com.im.portal.file.vo.FileVO;

@Controller
public class FileApi {

    @Resource(name = "FileService")
    private FileService fileService;

    public void matrixTime(int delayTime) {
        long saveTime = System.currentTimeMillis();
        long currTime = 0;
        while (currTime - saveTime < delayTime) {
            currTime = System.currentTimeMillis();
        }
    }

    /**
     * Build Facility Process
     */
    @Inject
    private FileSystemResource fsResource;

    @RequestMapping(value = "/api/file")
    public ModelAndView videoProcess(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fileupload") MultipartFile file, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/file/fileProcess");
            FileVO item = new FileVO();
            if (request.getMethod().equals("POST")) {
            } else { // get
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        }
        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/fileProcess")
    public ModelAndView fileProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/file/fileProcess");
            FileVO item = new FileVO();
            if (request.getMethod().equals("POST")) {
            } else { // get
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        }
        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/fileDownload/{fileSeq}")
    public void fileDownload(HttpServletRequest request, HttpServletResponse response,
            @PathVariable(value = "fileSeq") String fileSeq) throws Exception {
        FileVO item = new FileVO();
        if (!this.checkNumber(fileSeq)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        item.setFileSeq(Integer.parseInt(fileSeq));
        item.setFilePath("");
        FileVO metadataRes = fileService.MedatadataInfo(item);
        if (metadataRes == null) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }

        String filePath = fsResource.getPath() + metadataRes.getFilePath();
        if (!isValidFilePath(filePath)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        File downfile = new File(filePath);
        long L = downfile.length();
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }
        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            byte[] outByte = new byte[(int) L];
            while (inputStream.read(outByte, 0, (int) L) != -1) {
                outStream.write(outByte, 0, (int) L);
            }
        } catch (Exception e) {
            throw new IOException();
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    @RequestMapping(value = "/api/imgUrl")
    public void fileDownload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        FileVO item = new FileVO();

        String filePath = request.getParameter("filePath");
        String oriName = CommonFnc.emptyCheckString("oriName", request);

        String widthStr = CommonFnc.emptyCheckString("width", request);
        String heightStr = CommonFnc.emptyCheckString("height", request);

        if (oriName != null && !oriName.isEmpty()) {
            if (!isValidFileName(oriName)) {
                response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return;
            }
        }

        if (!widthStr.equals("") && !heightStr.equals("")) {
            String[] pathStrArr = filePath.split("/");
            String fName = pathStrArr[pathStrArr.length -1];
            String name = fName.substring(0, fName.lastIndexOf("."));
            String ext = fName.substring(fName.lastIndexOf(".") + 1, fName.length());

            String filePathWithSize = "";
            for (int i = 0; i < pathStrArr.length - 1; i++) { // 마지막 인자는 파일 명으로 제외
                if (i == 0) {
                    filePathWithSize += "/";
                }

                if (!pathStrArr[i].equals("")) {
                    filePathWithSize += pathStrArr[i] + "/";
                }
            }
            if (!filePathWithSize.equals("")) {
                filePathWithSize += name + "_" + widthStr + "x" + heightStr + "." + ext;
            }
            item.setFilePath(filePathWithSize);
        } else {
            item.setFilePath(filePath);
        }

        FileVO metadataRes = fileService.MedatadataInfo(item);

        if (metadataRes == null) {
            // 썸네일 이미지가 존재하지 않을 경우 원본 이미지 가져옴.
            if (!widthStr.equals("") && !heightStr.equals("")) {
                item.setFilePath(filePath);
                metadataRes = fileService.MedatadataInfo(item);

                if (metadataRes == null) {
                    metadataRes = new FileVO();
                    metadataRes.setFilePath(filePath);
                    metadataRes.setOrginlFileNm(oriName);
                }
            } else {
                metadataRes = new FileVO();
                metadataRes.setFilePath(filePath);
                metadataRes.setOrginlFileNm(oriName);
            }
        }

        String fullFilePath = fsResource.getPath() + metadataRes.getFilePath();
        if (!isValidFilePath(fullFilePath)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        File downfile = new File(fullFilePath);
        long L = downfile.length();
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }
        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            byte[] outByte = new byte[(int) L];
            while (inputStream.read(outByte, 0, (int) L) != -1) {
                outStream.write(outByte, 0, (int) L);
            }
        } catch (Exception e) {
            throw new IOException();
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    public boolean checkNumber(String value) {
        if (value == null) {
            return false;
        }

        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isAllowUploadFileType(String fileType) {
        boolean isAllow = false;
        String[] allowFileTypeArr = { "hwp", "txt", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "png", "jpg", "jpeg", "bmp", "gif", "mp4", "xml", "zip", "exe" };

        for (String allowFileType : allowFileTypeArr) {
            if (allowFileType.equalsIgnoreCase(fileType)) {
                isAllow = true;
                break;
            }
        }

        return isAllow;
    }

    public boolean isValidFileName(String fileName) {
        if (fileName == null || fileName.isEmpty() || fileName.length() > 50) {
            return false;
        }

        if ((fileName.indexOf("\0")) != -1 || (fileName.indexOf(";")) != -1 || (fileName.indexOf("./")) != -1 || (fileName.indexOf(".\\")) != -1) {
            return false;
        }

        String fileExt = fileName.substring(fileName.lastIndexOf('.' ) + 1);
        if (fileExt.equals("")) {
            return false;
        }

        if (!isAllowUploadFileType(fileExt)) {
            return false;
        }

        return true;
    }

    public boolean isValidFilePath(String filePath) {
        if (filePath == null || filePath.isEmpty()) {
            return false;
        }

        if ((filePath.indexOf("\0")) != -1 || (filePath.indexOf(";")) != -1 || (filePath.indexOf("..")) != -1
                || (filePath.indexOf("./")) != -1 || (filePath.indexOf(".\\")) != -1 || (filePath.indexOf(":")) != -1) {
            return false;
        }

        String fileExt = filePath.substring(filePath.lastIndexOf('.' ) + 1);
        if (fileExt.equals("")) {
            return false;
        }

        if (!isAllowUploadFileType(fileExt)) {
            return false;
        }

        return true;
    }
}
