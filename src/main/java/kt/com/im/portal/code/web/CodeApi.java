/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.code.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.portal.code.service.CodeService;
import kt.com.im.portal.code.vo.CodeVO;
import kt.com.im.portal.common.util.CommonFnc;
import kt.com.im.portal.common.util.ResultModel;

/**
 *
 * 코드 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.08
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 08.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class CodeApi {

    private final static String viewName = "../resources/api/code/codeProcess";

    @Resource(name = "CodeService")
    private CodeService codeService;

    /**
     * 공통 코드 관련 처리 API
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param comnCdCtg - 조건값 : 코드 분류
     * @param comnCdValue - 조건값 : 코드 값
     * @return modelAndView & API 메세지값 & WebStatus
     *         + GET - 검색조건에 따른 코드 리스트
     */
    @RequestMapping(value = "/api/codeInfo")
    public ModelAndView CodeInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);

        if (method.equals("GET")) {
            boolean isList = (request.getParameter("comnCdSeq") == null);
            if (isList)
                return this.getCodeList(request, rsModel);

            return this.getCode(request, rsModel);
        }

        rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getCodeList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String comnCdCtg = request.getParameter("comnCdCtg") == null ? "" : request.getParameter("comnCdCtg");          // 코드 분류
        String comnCdValue = request.getParameter("comnCdValue") == null ? "" : request.getParameter("comnCdValue");    // 코드
        String verify = request.getParameter("verify") == null ? "" : request.getParameter("verify");                   // 코드 값
        String remoteCodes = request.getParameter("remoteCode") == null ? "" : request.getParameter("remoteCode");      // 제외코드
        String display = request.getParameter("display") == null ? "" : request.getParameter("display");                // 코드 값
        String delYn = request.getParameter("delYn") == null ? "N" : request.getParameter("delYn");                     // 코드 값
        String sttusVal = CommonFnc.emptyCheckString("sttusVal", request);

        CodeVO item = new CodeVO();
        item.setComnCdCtg(comnCdCtg);
        item.setComnCdValue(comnCdValue);
        item.setDelYn(delYn);
        item.setVerify(verify);
        item.setDisplay(display);
        item.setSttusVal(sttusVal);

        String[] remoteCode = remoteCodes.split(",");

        List<String> list = new ArrayList<String>();
        for (int i = 0; i < remoteCode.length; i++) {
            list.add(remoteCode[i]);
        }
        item.setList(list);

        List<CodeVO> resultItem = codeService.getCodeList(item);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("codeList");
        rsModel.setData("item", resultItem);

        return rsModel.getModelAndView();
    }

    private ModelAndView getCode(HttpServletRequest request, ResultModel rsModel) throws Exception {
        CodeVO vo = new CodeVO();
        vo.setComnCdSeq(Integer.parseInt(request.getParameter("comnCdSeq")));

        CodeVO resultItem = codeService.getCode(vo);
        if (resultItem == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("codeInfo");
        rsModel.setData("item", resultItem);

        return rsModel.getModelAndView();
    }

    /**
     * 공통 코드 구분 목록 처리 API
     * @param comnCdCtg - 조건값 : 코드 분류
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/codeSecInfo")
    public ModelAndView CodeSectionInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            return this.getCodeSectionList(request, rsModel);
        }

        rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getCodeSectionList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        CodeVO vo = new CodeVO();
        List<CodeVO> resultItem = codeService.getCodeSectionList(vo);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("codeList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

}
