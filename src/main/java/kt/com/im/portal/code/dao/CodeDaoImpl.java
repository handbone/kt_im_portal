/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.code.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.portal.code.vo.CodeVO;
import kt.com.im.portal.common.dao.mysqlAbstractMapper;

/**
*
* 클래스에 대한 상세 설명
*
* @author A2TEC
* @since 2018.06.08
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 06. 08.   A2TEC      최초생성
*
*
* </pre>
*/

@Repository("CodeDao")
@Transactional
public class CodeDaoImpl extends mysqlAbstractMapper implements CodeDao {

    /**
     * 코드 리스트 정보 조회
     * 
     * @param CodeVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public List<CodeVO> getCodeList(CodeVO vo) throws Exception {
        return selectList("CodeDAO.selectCodeList", vo);
    }

    /**
     * 코드  정보 조회
     * 
     * @param CodeVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public CodeVO getCode(CodeVO vo) throws Exception {
        return selectOne("CodeDAO.selectCode", vo);
    }

    /**
     * 최상위 코드 리스트 정보
     * 
     * @param CodeVO
     * @return 최상위 코드 리스트 정보
     */
    @Override
    public List<CodeVO> getCodeSectionList(CodeVO vo) throws Exception {
        return selectList("CodeDAO.selectCodeSectionList", vo);
    }
}
