/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.portal.notice.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.portal.common.util.CommonFnc;
import kt.com.im.portal.common.util.ResultModel;
import kt.com.im.portal.file.service.FileService;
import kt.com.im.portal.file.vo.FileVO;
import kt.com.im.portal.notice.service.NoticeService;
import kt.com.im.portal.notice.vo.NoticeVO;

/**
 *
 * 공지사항 관련 처리 API
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 06. 25.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class NoticeApi {

    @Resource(name="NoticeService")
    private NoticeService noticeService;

    @Resource(name="FileService")
    private FileService fileService;

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.KOREAN);
    Date date = new Date();

    /**
     * API WEB
     * Web Process*/
    @RequestMapping(value = "/api/notice")
    public ModelAndView noticeProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/customer/notice/noticeProcess");
        request.setCharacterEncoding("UTF-8");

        NoticeVO item = new NoticeVO();
        FileVO fileVO = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int noticeSeq = CommonFnc.emptyCheckInt("noticeSeq", request);

            if (noticeSeq != 0) {
                item.setNoticeSeq(noticeSeq);
                fileVO.setFileContsSeq(noticeSeq);
                fileVO.setFileSe("NOTICE");
                List<FileVO> flist = fileService.fileList(fileVO);

                NoticeVO resultItem = noticeService.noticeDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (등록자 이름) */
                    resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                    noticeService.updateNoticeRetvNum(item);
                    resultItem.setRetvNum(resultItem.getRetvNum() + 1); 
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("fileList",flist);
                    rsModel.setResultType("noticeDetail");
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (noticeSeq == 0 && CommonFnc.checkReqParameter(request, "noticeSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                // 중요 공지사항 리스트 조회
                List<NoticeVO> imptNoticeList = noticeService.noticeImptList(item);
                int imptTotCnt = noticeService.noticeImptListTotalCount(item);

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }

                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 13);
                // 중요 공지사항은 최대 3개까지 지정 가능하므로 한 페이지에 표시할 row 갯수는 13로 하여 조회 쿼리에서는 10개 단위로 조회함.
                limit = limit - 3;
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("num")) {
                    sidx="NOTICE_SEQ";
                } else if (sidx.equals("noticeTitle")) {
                    sidx="NOTICE_TITLE";
                } else if (sidx.equals("cretrNm")) {
                    sidx="CRETR_NM";
                } else if (sidx.equals("noticeType")) {
                    sidx="NOTICE_TYPE";
                } else if (sidx.equals("retvNum")) {
                    sidx="RETV_NUM";
                } else if(sidx.equals("fileExist")) {
                    sidx="FILE_COUNT";
                } else {
                    sidx="NOTICE_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<NoticeVO> noticeList = noticeService.noticeList(item);

                List<NoticeVO> resultItem = new ArrayList<NoticeVO>();
                resultItem.addAll(imptNoticeList);
                resultItem.addAll(noticeList);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                }else{
                    int totalCount = noticeService.noticeListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    // 전체 갯수는 중요공지사항을 포함하여 계산하고 totalpage 계산에서는 중요공지사항 갯수 제외, 이는 중요공지사항은 항상 표시하기 때문
                    rsModel.setData("imptTotCnt", imptTotCnt);
                    rsModel.setData("totalCount", totalCount + imptTotCnt);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("noticeList");
                }
            }
        }

        return rsModel.getModelAndView();
    }
}
